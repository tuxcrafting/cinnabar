# GCC toolchain target
TARGET = riscv64-linux-gnu

# Board to compile for
# - riscv64_virtio
BOARD = riscv64_virtio

ARCH = $(firstword $(subst _, ,$(BOARD)))

# Object directory
OBJ = obj-$(BOARD)

GCC = $(TARGET)-gcc
GNAT = $(TARGET)-gnat
OBJCOPY = $(TARGET)-objcopy
AR = $(TARGET)-ar

GNATMAKE = $(GNAT) make
GNATBIND = $(GNAT) bind

system_RTS = $(firstword $(wildcard /usr/lib/gcc*/$(TARGET)/10))

common_flags = -g -Os -flto -fno-pic -fno-stack-protector

ifeq ($(ARCH), riscv64)
common_flags += -march=rv64imafdc -mabi=lp64d -mcmodel=medany
endif

# Language flags
CFLAGS = $(common_flags)
ASFLAGS = $(common_flags)
ADAFLAGS = $(common_flags) -gnat2012
LDFLAGS = $(common_flags) -static -nostdlib

GNATMAKEFLAGS = -j0

mkdir = mkdir -p $(shell dirname $@)
ln = ln -sf $(abspath $<) $@

all: $(OBJ)/cmk.bin

$(OBJ)/%.c.o: %.c
	@$(mkdir)
	$(GCC) $(CFLAGS) -c -o $@ $<
$(OBJ)/%.s.o: %.s
	@$(mkdir)
	$(GCC) $(ASFLAGS) -c -o $@ $<

.PHONY: run-qemu clean

# Init process

init_src = init init/$(ARCH)
init_ada_src = $(shell find $(init_src) -maxdepth 1 -regex '.*\.ad[bs]')
init_non_ada_src = $(shell find $(init_src) -maxdepth 1 -regex '.*\.[cs]')
init_non_ada_obj = $(init_non_ada_src:%=$(OBJ)/%.o)
init_lds = init/$(ARCH)/linker.ld

ifeq ($(ARCH), riscv64)
init_objcopy_flags = -O elf64-littleriscv
endif

$(OBJ)/init/init.elf: $(init_ada_src) $(init_non_ada_obj) $(init_lds)
	@$(mkdir)
	# $(GNATMAKE) $(ADAFLAGS) $(GNATMAKEFLAGS) -a \
	# 	-D $(OBJ)/init $(init_src:%=-aI%) init.adb \
	# 	-o $@ -nostdlib \
	# 	-largs -T $(init_lds) $(LDFLAGS) \
	# 	$(init_non_ada_obj) -lgcc
	# rm -f b~init.*
	$(GCC) -o $@ -T $(init_lds) $(LDFLAGS) -lgcc $(init_non_ada_obj)

$(OBJ)/init/init.bin: $(OBJ)/init/init.elf
	python3 init/mkbin.py $< $@

$(OBJ)/init/init_data.o: $(OBJ)/init/init.bin
	cd $(OBJ)/init; \
	$(OBJCOPY) -I binary $(init_objcopy_flags) \
	--rename-section .data=.rodata,alloc,load,readonly,data,contents \
	init.bin init_data.o

# Kernel

ifeq ($(ARCH), riscv64)
kernel_bits = 64
endif

kernel_src = kernel
ifeq ($(BOARD), riscv64_virtio)
kernel_src += kernel/riscv64_virtio
endif
ifeq ($(ARCH), riscv64)
kernel_src += kernel/riscv kernel/riscv64
endif

kernel_ada_src = $(shell find $(kernel_src) -maxdepth 1 -regex '.*\.ad[bs]')
kernel_non_ada_src = $(shell find $(kernel_src) -maxdepth 1 -regex '.*\.[cs]')
kernel_non_ada_obj = $(kernel_non_ada_src:%=$(OBJ)/%.o) $(OBJ)/init/init_data.o
kernel_lds = kernel/$(BOARD)/linker.ld
kernel_gen_s = $(shell echo kernel/*.m4 | xargs -n 1 basename)
kernel_gen = $(kernel_gen_s:%.m4=$(OBJ)/kernel/gen/%)

$(OBJ)/kernel/gen/%: defs/$(ARCH).m4 defs/common.m4 kernel/%.m4
	@$(mkdir)
	m4 $^ > $@

$(OBJ)/cmk.elf: $(kernel_ada_src) $(kernel_non_ada_obj) \
		$(kernel_lds) $(kernel_gen)
	@$(mkdir)
	$(GNATMAKE) $(ADAFLAGS) $(GNATMAKEFLAGS) -a \
		-gnatec=kernel/gnat.adc \
		-gnatep=kernel/gnatprep.txt \
		-gnateDBits=$(kernel_bits) \
		-D $(OBJ)/kernel $(kernel_src:%=-aI%) \
		-aI$(OBJ)/kernel/gen kernel.adb \
		-o $@ -nostdlib \
		-largs -T $(kernel_lds) $(LDFLAGS) \
		$(kernel_non_ada_obj) -lgcc
	rm -f b~kernel.*

$(OBJ)/cmk.bin: $(OBJ)/cmk.elf
	$(OBJCOPY) -O binary $< $@

# Running

ifeq ($(BOARD), riscv64_virtio)
QEMU = qemu-system-riscv64 -M virt -m 256M \
	-bios default -serial stdio \
	-device loader,addr=0x80200000,file=$<
endif

run-qemu: $(OBJ)/cmk.bin
	$(QEMU)

clean:
	rm -rf $(OBJ)
