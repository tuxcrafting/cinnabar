# Take an ELF and regurgitate a flat binary with some metadata for CMK
# to load and execute.

import struct
import sys

from elftools.elf.elffile import ELFFile

def elf2cmkbin(infile, outfile):
    elf = ELFFile(infile)
    text_len = rodata_len = data_len = bss_len = 0
    text_data = rodata_data = data_data = b""
    for s in elf.iter_sections():
        if s.name == ".text":
            text_len = s.header["sh_size"]
            text_data = s.data()
        elif s.name == ".rodata":
            rodata_len = s.header["sh_size"]
            rodata_data = s.data()
        elif s.name == ".data":
            data_len = s.header["sh_size"]
            data_data = s.data()
        elif s.name == ".bss":
            bss_len = s.header["sh_size"]
    header = struct.pack(
        "<LLLLL",
        text_len, rodata_len, data_len,
        bss_len, elf.header["e_entry"])
    full_bin = header + text_data + rodata_data + data_data
    outfile.write(full_bin)

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: mkbin.py INPUT OUTPUT")

    with open(sys.argv[1], "rb") as i:
        with open(sys.argv[2], "wb") as o:
            elf2cmkbin(i, o)
