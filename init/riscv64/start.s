	.section ".text"

	.global _start
_start:
	li a0, 0
	li t0, 0x001000
	la t1, hello
1:	lbu t2, 0(t1)
	addi t1, t1, 1
	bnez t2, 2f
	ebreak
2:	sd t2, 0(t0)
	ecall
	j 1b

	.section ".rodata"
hello:	.asciz "Hello from init process!!\n"
