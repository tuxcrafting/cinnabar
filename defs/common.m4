divert(-1)
define(`CAP_TYPES',`Nil,Untyped,Table,Page,Page_Table,Process,Scheduling_Context')
define(`CAP_SLOT_BITS',ifelse(BITS,`32',`5',`6'))
define(`CAP_PROCESS_BITS',`10')
define(`SCHEDULER_MAX_PRIO',`0')
define(`ERRNO',`Success,Nil_Capability,Non_Nil_Capability,Invalid_Capability,Not_Authorized,Bounds,Invalid_Type,Invalid_Size,Final,Bad_Guard,Invalid_Value')
define(`SYSCALLS',`Debug_Putchar')
divert(0)dnl
