# Cinnabar

Cinnabar is a hobby operating system.

## Building

First, ensure you have all the dependencies:

- GCC 10 with support for C and Ada
- GNU binutils
- GNU make
- GNU m4
- Python 3
- pyelftools

Then, building is done simply through `make`. The possible variables
that can be changed are:

- `TARGET` - GCC target.
- `BOARD` - Platform to build Cinnabar for, valid options are shown in
  the GNUmakefile.

Default `all` target builds the kernel, there is also the `run-qemu`
target to run it in QEMU.
