--  Capability-related types and definitions.

with Capability_Type;
with Errno;
with Interfaces;              use Interfaces;
with System;                  use System;
with System.Storage_Elements; use System.Storage_Elements;

package Capability is
   package T renames Capability_Type;

   Slot_Bits          : constant := T.Slot_Bits;
   Process_Bits       : constant := T.Process_Bits;
   Scheduler_Max_Prio : constant := T.Scheduler_Max_Prio;

   --  Maximum capability pointer bits that a single Table capability
   --  can address.
   Max_Table_Bits : constant := 30 - Slot_Bits;

   function "="
     (A, B : T.Typ)
     return Boolean renames T."=";
   function "="
     (A, B : Errno.Typ)
     return Boolean renames Errno."=";

   --  Capability pointer. Represents a capability within a capability
   --  table, potentially recursively accessing multiple capability
   --  tables.
   --
   --  Starting from the most significant bit and the root capability
   --  table, first Guard_Bits are taken and checked to be the same as
   --  Guard. Then, Table_Bits are taken and used to address within
   --  Table. This continues until the translation stops, either
   --  because it addressed a non-Cap_Table capability, there are no
   --  pointer bits left, or because it hit a depth limit.
   type Pointer is new Unsigned_32;

   --  Number of bits within an address.
   subtype Address_Bits is Natural range 0 .. Word_Size;
   --  Number of bits within a capability pointer.
   subtype Pointer_Bits is Natural range 0 .. 32;

   --  Page table level value for an undefined level, and page table
   --  index for unmapped pages and page tables.
   Pt_Level_Undef, Pt_Index_Unmapped : constant Natural := Natural'Last;

   type Slot (Typ : T.Typ := T.Nil) is
      record
         --  Whether the capability is the first child of its parent -
         --  see Prev.
         Fst_Child   : Boolean;
         --  Whether the capability is readable/writable.
         Read, Write : Boolean;
         --  The capability is being deleted and shouldn't be
         --  recursively deleted.
         Deleting    : Boolean;

         --  Previous capability in the linked list of children, or if
         --  Fst_Child is set, parent capability of the linked list.
         Prev  : access Slot;
         --  Next capability in the linked list of children.
         Next  : access Slot;
         --  First child in the linked list of children.
         Child : access Slot;

         case Typ is
            when T.Nil =>
#if Bits = "32" then
--  TODO
#elsif Bits = "64" then
               Pad : Storage_Array (1 .. 32);
#end if;

            when T.Untyped =>
               --  Base address. Aligned to Bits bits. Physical
               --  address.
               Un_Addr : Address;
               --  Bits of address represented by this capability.
               Un_Bits : Address_Bits;

            when T.Table =>
               --  Address of the capability table. Virtual address.
               Ta_Table      : Address;
               --  Pointer guard.
               Ta_Guard      : Pointer;
               --  Capability pointer bits to address within the
               --  table.
               Ta_Table_Bits : Pointer_Bits;
               --  Capability pointer bits for the guard.
               Ta_Guard_Bits : Pointer_Bits;

            when T.Page =>
               --  Address of the page. Physical address.
               Pa_Addr        : Address;
               --  Address of the page table in which the page is
               --  mapped. Physical address.
               Pa_Table       : Address;
               --  Architecture-specific page level.
               Pa_Level       : Natural;
               --  Index within the mapping page table.
               Pa_Table_Index : Natural;

            when T.Page_Table =>
               --  Address of the page table. Physical address.
               Pt_Addr        : Address;
               --  Address of the page table in which the page table
               --  is itself mapped. Physical address.
               Pt_Table       : Address;
               --  Architecture-specific page table level.
               Pt_Level       : Natural;
               --  Index within the mapping page table.
               Pt_Table_Index : Natural;

            when T.Process =>
               --  Address of the process data. Virtual address.
               Pr_Addr : Address;

            when T.Scheduling_Context =>
               --  Address of the scheduler data. Virtual address.
               Sc_Addr     : Address;
               --  Maximum priority accessible by this capability.
               Sc_Max_Prio : Natural;
         end case;
      end record;

#if Bits = "32" then
--  TODO
#elsif Bits = "64" then
   for Slot use
      record
         Typ       at 0 range 0 .. 4;
         Fst_Child at 0 range 5 .. 5;
         Read      at 0 range 6 .. 6;
         Write     at 0 range 7 .. 7;
         Deleting  at 0 range 8 .. 8;

         Prev  at  8 range 0 .. 63;
         Next  at 16 range 0 .. 63;
         Child at 24 range 0 .. 63;

         --  Nil
         Pad at 32 range 0 .. 255;

         --  Untyped
         Un_Addr at 32 range 0 .. 63;
         Un_Bits at 40 range 0 .. 15;

         --  Table
         Ta_Table      at 32 range 0 .. 63;
         Ta_Guard      at 40 range 0 .. 31;
         Ta_Table_Bits at 44 range 0 .. 15;
         Ta_Guard_Bits at 46 range 0 .. 15;

         -- Page
         Pa_Addr        at 32 range 0 .. 63;
         Pa_Table       at 40 range 0 .. 63;
         Pa_Level       at 48 range 0 .. 31;
         Pa_Table_Index at 52 range 0 .. 31;

         -- Page_Table
         Pt_Addr        at 32 range 0 .. 63;
         Pt_Table       at 40 range 0 .. 63;
         Pt_Level       at 48 range 0 .. 31;
         Pt_Table_Index at 52 range 0 .. 31;

         -- Process
         Pr_Addr at 32 range 0 .. 63;
      end record;
   for Slot'Size use 512;
   #end if;

   --  Array of capability slots, as in a Table capability.
   type Slot_Array is array (Natural range <>) of aliased Slot;

   --  Access type for a capability slot.
   type Slot_Access is access all Slot;
   for Slot_Access'Storage_Size use 0;

   --  Emtpy capability slot.
   Empty_Slot : constant Slot :=
     (T.Nil, False, False, False, False, null, null, null, (others => 0));
end Capability;
pragma Preelaborate (Capability);
