--  Architecture-specific paging-related functions.

with System; use System;

package Capability.Page_Table.Arch is
   --  Change a mapping in a page table.
   procedure Map
     (Table, Addr : Address;
      Typ         : Mapping_Type;
      Index       : Natural);

   --  Activate a page table.
   procedure Activate
     (Table : Address);

   --  Initialize a page table by level.
   procedure Init_Pt
     (Table : Address;
      Level : Natural);

   --  Check if the index is valid for a given page level.
   function Valid_Index
     (Level, Index : Natural)
     return Boolean;
end Capability.Page_Table.Arch;
pragma Preelaborate (Capability.Page_Table.Arch);
