--  Page table functions.

with Errno;
with System; use System;

package Capability.Page_Table is
   type Mapping_Type is
     (None, Page_Table, Page_R, Page_Rw, Page_Rx, Page_W);

   --  Map/remap/unmap an entry in a page table.
   procedure Map_Addr
     (Table, Addr, Slot_Addr : Address;
      Typ                    : Mapping_Type;
      Index                  : Natural);

   --  Unmap an entry in a page table (wrapper over Map_Addr).
   procedure Unmap_Addr
     (Table : Address;
      Index : Natural);

   --  Update a capability's address in a page table.
   procedure Update_Addr
     (Table, Cap_Addr : Address;
      Index           : Natural);

   --  Activate a page table.
   procedure Activate
     (Table : Address);

   --  Set the level of an uninitialized page table capability.
   --
   --  - Invalid_Capability - Cap is not a Page_Table.
   --  - Not_Authorized - Cap is not writable.
   --  - Final - Capability has already been initialized.
   --  - Invalid_Value - Level is not in the range
   --    1 .. Page_Table_Levels.
   function Set_Level
     (Cap   : aliased in out Slot;
      Level :                Natural)
     return Errno.Typ;

   --  Map/remap/unmap a page or page table capability into the page
   --  table.
   --
   --  - Invalid_Capability - Cap is not a Page_Table, Mapped is not a
   --    Page or Page_Table, or Mapped is not of a level exactly 1
   --    under Cap.
   --  - Not_Authorized - Cap is not writable or Mapped has
   --    insufficient permissions for the desired mapping type.
   --  - Bounds - Index is outside the possible mapping indices.
   --  - Final - Mapped has already been mapped in another page table,
   --    or there is already a mapping of another page or page table
   --    at the desired index.
   function Map
     (Cap, Mapped : aliased in out Slot;
      Typ         :                Mapping_Type;
      Index       :                Natural)
     return Errno.Typ;
end Capability.Page_Table;
pragma Preelaborate (Capability.Page_Table);
