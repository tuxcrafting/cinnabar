with Console; use Console;
with System.Address_Image;

package body Capability.Debug is
   procedure Print
     (Cap   : aliased Slot;
      Level :         Natural := 0) is

      procedure Put_Pointer is new Put_Unsigned (Num => Pointer);
   begin
      for Level_I in 1 .. Level loop
         Put ("  ");
      end loop;

      Put (T.Typ'Image (Cap.Typ) & " ");
      Put (if Cap.Read then "R" else "r");
      Put (if Cap.Write then "W" else "w");
      Put (" ");

      case Cap.Typ is
         when T.Nil =>
            New_Line;

         when T.Untyped =>
            Put (System.Address_Image (Cap.Un_Addr) & " ");
            Put ("(" & Address_Bits'Image (Cap.Un_Bits) & ")");
            New_Line;

         when T.Table =>
            Put (System.Address_Image (Cap.Ta_Table) & " ");
            Put ("(" & Address_Bits'Image (Cap.Ta_Table_Bits) & ") - ");
            Put_Pointer (Cap.Ta_Guard, Cap.Ta_Guard_Bits, 2);
            Put (" ");
            Put ("(" & Pointer_Bits'Image (Cap.Ta_Guard_Bits) & ")");
            New_Line;

         when T.Page =>
            Put (System.Address_Image (Cap.Pa_Addr) & " ");
            Put ("(" & Natural'Image (Cap.Pa_Level) & ") in ");
            Put (System.Address_Image (Cap.Pa_Table) & " ");
            Put ("(" & Natural'Image (Cap.Pa_Table_Index) & ")");
            New_Line;

         when T.Page_Table =>
            Put (System.Address_Image (Cap.Pt_Addr) & " ");
            Put ("(" & Natural'Image (Cap.Pt_Level) & ") in ");
            Put (System.Address_Image (Cap.Pt_Table) & " ");
            Put ("(" & Natural'Image (Cap.Pt_Table_Index) & ")");
            New_Line;

         when T.Process =>
            New_Line;

         when T.Scheduling_Context =>
            Put (System.Address_Image (Cap.Sc_Addr) & " ");
            Put ("(1 .. " & Positive'Image (Cap.Sc_Max_Prio) & ")");
            New_Line;
      end case;

      declare
         Child : access Slot := Cap.Child;
      begin
         while Child /= null loop
            Print (Child.all, Level + 1);
            Child := Child.Next;
         end loop;
      end;
   end Print;
end Capability.Debug;
