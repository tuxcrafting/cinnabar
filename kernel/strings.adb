package body Strings is
   function Index
     (Haystack, Needle : String)
     return Natural is

      Start_I  : Natural;
      Needle_I : Natural := Needle'First;
   begin
      for I in Haystack'Range loop
         if Haystack (I) = Needle (Needle_I) then
            if Needle_I = Needle'First then
               Start_I := I;
            end if;

            if Needle_I = Needle'Last then
               return Start_I;
            end if;

            Needle_I := Needle_I + 1;
         else
            Needle_I := Needle'First;
         end if;
      end loop;

      return 0;
   end Index;
end Strings;
