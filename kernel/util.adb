with Defs;
with System.Storage_Elements; use System.Storage_Elements;

package body Util is
   function To_Phy
     (Addr : Address)
     return Address is
     (To_Address (To_Integer (Addr) - Defs.Vma_Base));
   function To_Virt
     (Addr : Address)
     return Address is
     (To_Address (To_Integer (Addr) + Defs.Vma_Base));
end Util;
