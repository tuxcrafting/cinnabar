package Capability_Type is
   type Typ is (CAP_TYPES);

   Slot_Bits          : constant := CAP_SLOT_BITS;
   Process_Bits       : constant := CAP_PROCESS_BITS;
   Scheduler_Max_Prio : constant := SCHEDULER_MAX_PRIO;
end Capability_Type;
pragma Preelaborate (Capability_Type);
pragma Pure (Capability_Type);
