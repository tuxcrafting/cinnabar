--  Untyped capability functions.

with Errno;
with System; use System;

package Capability.Untyped is
   --  Make an untyped capability.
   procedure Make
     (Cap  : aliased in out Slot;
      Addr :                Address;
      Bits :                Address_Bits);

   --  Make Num capabilities of type Typ with Bits address bits out of
   --  a Untyped capability Cap, starting from the index From in
   --  the table Table.
   --
   --  Possible target types are:
   --
   --  Untyped: There is no restriction on Bits, other than it must
   --  fit inside the source capability's size.
   --
   --  Table: Bits must be at least Slot_Bits and less than or equal
   --  to Slot_Bits + Max_Table_Bits. The default guard is 0 bits.
   --
   --  Page: Bits must be an allowed value within Page_Level_Bits. The
   --  level is deduced from the page size.
   --
   --  Page_Table: Bits must be Page_Table_Bits - 1. The level must be
   --  set before the page table can be used.
   --
   --  Process: Bits must be Process_Bits.
   --
   --  - Non_Nil_Capability - One of the target slots in Table is not
   --    nil.
   --  - Invalid_Capability - Cap is not an Untyped capability, or
   --    Table is not a Table capability.
   --  - Not_Authorized - Table doesn't have write permissions.
   --  - Bounds - From .. From + Num is out of the bounds of Table, or
   --    Num is 0.
   --  - Invalid_Type - Typ is not a valid derivable type.
   --  - Invalid_Size - Bits is not a valid value for Typ, or (Num *
   --    (1 << Bits)) > (1 << Cap.Bits).
   --  - Final - Cap already has children.
   function Retype
     (Cap       : aliased in out Slot;
      Table     : aliased        Slot;
      Typ       :                T.Typ;
      From, Num :                Natural;
      Bits      :                Address_Bits)
     return Errno.Typ;
end Capability.Untyped;
pragma Preelaborate (Capability.Untyped);
