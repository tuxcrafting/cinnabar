with System.Storage_Elements; use System.Storage_Elements;

package Defs is
   type Page_Level_Bits_Type is
     array (Natural range <>) of Natural;

   Vma_Base          : constant := `16#'VMA_BASE`#';
   Phy_Bits          : constant := PHY_BITS;
   Page_Table_Bits   : constant := PAGE_TABLE_BITS;
   Page_Table_Length : constant := PAGE_TABLE_LENGTH;
   Page_Table_Levels : constant := PAGE_TABLE_LEVELS;

   Page_Level_Bits : constant Page_Level_Bits_Type := (PAGE_LEVEL_BITS);
end Defs;
pragma Preelaborate (Defs);
pragma Pure (Defs);
