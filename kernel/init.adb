with Capability.Base;
with Capability.Debug;
with Capability.Untyped;
with Console;                 use Console;
with Errno;
with Halt;
with Hardware_List;
with Init.Arch;
with Memory;                  use Memory;
with System.Address_Image;
with System.Storage_Elements; use System.Storage_Elements;
with Util;

package body Init is
   procedure Finish is
      Ignore : Errno.Typ;
   begin
      --  Reserve kernel memory.
      declare
         Kernel_Start, Kernel_End : Address;
      begin
         Get_Kernel_Start_End (Kernel_Start, Kernel_End);
         Hardware_List.Add_Reserved
           (Util.To_Phy (Kernel_Start), Kernel_End - Kernel_Start);
      end;

      --  Print available and reserved memory.
      Put_Line ("Available memory:");
      for Available_I in 1 .. Hardware_List.Available_Memory_N loop
         declare
            Mem_Range : Hardware_List.Memory_Range renames
              Hardware_List.Available_Memory (Available_I);
         begin
            Put_Line
              ("  " &
                 System.Address_Image (Mem_Range.Start) & "-" &
                 System.Address_Image (Mem_Range.Start + Mem_Range.Size));
         end;
      end loop;

      Put_Line ("Reserved memory:");
      for Reserved_I in 1 .. Hardware_List.Reserved_Memory_N loop
         declare
            Mem_Range : Hardware_List.Memory_Range renames
              Hardware_List.Reserved_Memory (Reserved_I);
         begin
            Put_Line
              ("  " &
                 System.Address_Image (Mem_Range.Start) & "-" &
                 System.Address_Image (Mem_Range.Start + Mem_Range.Size));
         end;
      end loop;

      Table_Cap :=
        (Capability.T.Table, False, True, True, False, null, null, null,
         Table_Data'Address, 0, 6, 0);

      Arch_Table_Cap := Table_Data (0)'Access;
      Process_Cap := Table_Data (1)'Access;
      Scheduler_Cap := Table_Data (2)'Access;

      declare
         pragma Suppress (Discriminant_Check);
      begin
         Scheduler_Cap.all :=
           (Capability.T.Scheduling_Context, False, True, True, False,
            null, null, null,
            Scheduler_Data'Address, Capability.Scheduler_Max_Prio);
      end;

      Arch.Init_Process;

      Ignore := Capability.Base.Clone (Table_Cap, Process_Data.Table);

      Make_Untyped (To_Address (0), 47);

      Capability.Debug.Print (Table_Cap);

      for I in 0 .. Table_I - 1 loop
         Capability.Debug.Print (Table_Data (I));
      end loop;

      declare
         Arch_Table_Data : aliased Capability.Slot_Array
           (0 .. 2 ** Arch_Table_Cap.Ta_Table_Bits - 1);
         for Arch_Table_Data'Address use Arch_Table_Cap.Ta_Table;
         pragma Import (Ada, Arch_Table_Data);
      begin
         for I in Arch_Table_Data'Range loop
            Capability.Debug.Print (Arch_Table_Data (I));
         end loop;
      end;

      Process_Data.Running := True;
      Process_Data.Waiting := False;
      Process_Data.Queued := False;
      Process_Data.Current := False;
      Capability.Process.Set_Scheduling_Context
        (Process_Data, Scheduler_Data'Access, Capability.Scheduler_Max_Prio);
      Capability.Process.Recheck (Process_Data);

      Capability.Scheduling_Context.Next (Scheduler_Data);
   end Finish;

   procedure Make_Untyped
     (Addr : Address;
      Bits : Capability.Address_Bits) is

      function Overlap
        (X1, X2, Y1, Y2 : Address)
        return Boolean;
      function Overlap
        (X1, X2, Y1, Y2 : Address)
        return Boolean is
        (X1 <= Y2 and then Y1 <= X2);

      function Is_Valid
        (Addr : Address;
         Size : Storage_Count)
        return Boolean;
      function Is_Valid
        (Addr : Address;
         Size : Storage_Count)
        return Boolean is
      begin
         for Available_I in
           1 .. Hardware_List.Available_Memory_N loop
            declare
               Mem_Range : Hardware_List.Memory_Range renames
                 Hardware_List.Available_Memory (Available_I);
            begin
               if Addr >= Mem_Range.Start
                 and then Addr + Size <= Mem_Range.Start + Mem_Range.Size
               then
                  goto Available;
               end if;
            end;
         end loop;

         return False;

         <<Available>>

         for Reserved_I in 1 .. Hardware_List.Reserved_Memory_N loop
            declare
               Mem_Range : Hardware_List.Memory_Range renames
                 Hardware_List.Reserved_Memory (Reserved_I);
            begin
               if Overlap
                 (Addr, Addr + Size, Mem_Range.Start,
                  Mem_Range.Start + Mem_Range.Size)
               then
                  return False;
               end if;
            end;
         end loop;

         return True;
      end Is_Valid;

      function Contains_Valid
        (Addr : Address;
         Size : Storage_Count)
        return Boolean;
      function Contains_Valid
        (Addr : Address;
         Size : Storage_Count)
        return Boolean is
      begin
         for Available_I in
           1 .. Hardware_List.Available_Memory_N loop
            declare
               Mem_Range : Hardware_List.Memory_Range renames
                 Hardware_List.Available_Memory (Available_I);
            begin
               if Overlap
                 (Addr, Addr + Size, Mem_Range.Start,
                  Mem_Range.Start + Mem_Range.Size)
               then
                  return True;
               end if;
            end;
         end loop;

         return False;
      end Contains_Valid;

      Size : constant Storage_Count := 2 ** Bits;
   begin
      --  Check if the number of bits is above the minimum.
      if Bits < Min_Untyped_Bits then
         return;
      end if;

      if Is_Valid (Addr, Size) then
         --  If the range is available and not reserved, make a
         --  capability.
         Capability.Untyped.Make (Table_Data (Table_I), Addr, Bits);
         Table_I := Table_I + 1;
      elsif Contains_Valid (Addr, Size) then
         --  Else, divide (and hopefully conquer).
         Make_Untyped (Addr, Bits - 1);
         Make_Untyped (Addr + 2 ** (Bits - 1), Bits - 1);
      end if;
   end Make_Untyped;
end Init;
