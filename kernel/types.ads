--  Helper types.

with System; use System;

package Types is
   --  System word type.
   type Word is mod 2 ** Word_Size;

   --  Word array.
   type Word_Array is
     array (Natural range <>) of aliased Word;

   --  Address array.
   type Address_Array is
     array (Natural range <>) of aliased Address;
end Types;
pragma Preelaborate (Types);
pragma Pure (Types);
