--  Init executable loader.

with Interfaces; use Interfaces;
with System;     use System;

package Init.Loader is
   type Header is
      record
         Rx_Len, R_Len, Rw_Len,
           Rw_Uninitialized_Len,
           Entry_Point : Unsigned_32;
      end record;

   for Header use
      record
         Rx_Len               at  0 range 0 .. 31;
         R_Len                at  4 range 0 .. 31;
         Rw_Len               at  8 range 0 .. 31;
         Rw_Uninitialized_Len at 12 range 0 .. 31;
         Entry_Point          at 16 range 0 .. 31;
      end record;
   for Header'Bit_Order use Low_Order_First;
   for Header'Scalar_Storage_Order use Low_Order_First;

   type Executable is
      record
         Header_Access            : access Header;
         Rx_Addr, R_Addr, Rw_Addr : Address;
      end record;

   procedure Load
     (Exec : out Executable);
end Init.Loader;
pragma Preelaborate (Init.Loader);
