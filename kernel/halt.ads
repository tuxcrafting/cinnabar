--  Halt the machine.
procedure Halt;
pragma No_Return (Halt);
pragma Preelaborate (Halt);
