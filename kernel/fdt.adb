with Console; use Console;
with Hardware_List;
with Memory;  use Memory;
with Strings; use Strings;
with System.Address_Image;

package body Fdt is
   procedure Init
     (Addr : Address) is

      Hdr : aliased Header;
      for Hdr'Address use Addr;
      pragma Import (Ada, Hdr);
   begin
      Put_Line ("Parsing FDT at " & System.Address_Image (Addr));

      --  Check if the FDT is valid.
      if Hdr.Magic /= Magic_Number then
         Put_Line ("Invalid FDT magic number");
         Panic;
      end if;

      if Compatible_Version < Hdr.Last_Comp_Version then
         Put_Line ("Incompatible FDT version");
         Panic;
      end if;

      Put_Line (" total_size =" & Unsigned_32'Image (Hdr.Total_Size));

      Parse_Memory_Map (Hdr);
   end Init;

   procedure Parse_Memory_Map
     (Hdr : aliased Header) is

      Iter_St : Iter.State;
      Depth   : Natural := 0;

      Addr_Cells, Size_Cells : Natural := 1;

      In_Memory, In_Reserved_Memory : Boolean := False;
   begin
      Iter.Init (Iter_St, Hdr);

      Token_Loop : loop
         declare
            Tok : Token;
         begin
            Iter.Next (Iter_St, Tok);
            declare
               Name : aliased constant String
                 (1 .. Natural (Tok.Name_L));
               for Name'Address use Tok.Name_A;
               pragma Import (Ada, Name);

               --  Value as an array of big-endian Unsigned_32, since
               --  it's all that's necessary for the parsed properties
               --  here.
               Value : aliased constant U32_Array
                 (1 .. Natural (Tok.Value_L) / 4);
               for Value'Address use Tok.Value_A;
               pragma Import (Ada, Value);
            begin
               case Tok.Typ is
                  when Tok_Begin_Node =>
                     declare
                        At_I : constant Natural := Index (Name, "@");
                     begin
                        if Depth = 1
                          and then At_I /= 0
                          and then Name (1 .. At_I) = "memory@"
                        then
                           In_Memory := True;
                        elsif Depth = 1
                          and then Name = "reserved-memory"
                        then
                           In_Reserved_Memory := True;
                        end if;
                     end;

                     Depth := Depth + 1;

                  when Tok_End_Node =>
                     In_Memory := False;
                     if Depth = 2 then
                        In_Reserved_Memory := False;
                     end if;

                     Depth := Depth - 1;

                  when Tok_Prop =>
                     if Depth = 1 then
                        if Name = "#address-cells" then
                           Addr_Cells := Natural (Value (Value'First));
                        elsif Name = "#size-cells" then
                           Size_Cells := Natural (Value (Value'First));
                        end if;
                     end if;

                     if In_Memory and then Name = "reg" then
                        Parse_Mem_Reg
                          (Value, Addr_Cells, Size_Cells,
                           Hardware_List.Add_Available'Access);
                     elsif In_Reserved_Memory and then Name = "reg" then
                        Parse_Mem_Reg
                          (Value, Addr_Cells, Size_Cells,
                           Hardware_List.Add_Reserved'Access);
                     end if;

                  when others =>
                     null;
               end case;
            end;
            exit Token_Loop when Tok.Typ = Tok_End;
         end;
      end loop Token_Loop;
   end Parse_Memory_Map;

   procedure Parse_Mem_Reg
     (Reg                    :        U32_Array;
      Addr_Cells, Size_Cells :        Natural;
      Callback               : access procedure
        (Start : Address;
         Size  : Storage_Count)) is

      Reg_I : Natural := Reg'First;
   begin
      while Reg_I <= Reg'Last loop
         declare
            Addr_N, Size_N : Unsigned_64 := 0;
         begin
            for Addr_I in 1 .. Addr_Cells loop
               Addr_N := Shift_Left (Addr_N, 32) +
                 Unsigned_64 (Reg (Reg_I + Addr_I - 1));
            end loop;
            Reg_I := Reg_I + Addr_Cells;

            for Size_I in 1 .. Size_Cells loop
               Size_N := Shift_Left (Size_N, 32) +
                 Unsigned_64 (Reg (Reg_I + Size_I - 1));
            end loop;
            Reg_I := Reg_I + Size_Cells;

            Callback
              (To_Address (Integer_Address (Addr_N)),
               Storage_Count (Size_N));
         end;
      end loop;
   end Parse_Mem_Reg;

   package body Iter is
      procedure Init
        (St  :         out State;
         Hdr : aliased     Header) is
      begin
         --  Passing an access type as parameter of Iter.Init and
         --  assigning it to St.Hdr causes a runtime error (which I
         --  assume is related to some sort of conversion, although
         --  I'm not sure what), but taking 'Unchecked_Access of an
         --  aliased works so whatever.
         St.Hdr  := Hdr'Unchecked_Access;
         St.Addr := Hdr'Address + Storage_Offset (Hdr.Off_Dt_Struct);
      end Init;

      procedure Next
        (St  : in out State;
         Tok :    out Token) is

         --  Token metadata as big-endian Unsigned_32's.
         Tok_Meta : aliased constant U32_Array (1 .. 3);
         for Tok_Meta'Address use St.Addr;
         pragma Import (Ada, Tok_Meta);

         --  Type of the token.
         Tok_Type : constant Token_Type :=
           Token_Type'Enum_Val (Tok_Meta (1));

         --  Length of the token.
         Tok_Len : Storage_Count;
      begin
         Tok.Typ := Tok_Type;
         case Tok_Type is
            when Tok_Begin_Node =>
               declare
                  --  Type is followed by a null-terminated string of
                  --  the node name.
                  Name_Addr : constant Address       := St.Addr + 4;
                  Name_Len  : constant Storage_Count := Strlen (Name_Addr);
               begin
                  Tok.Name_A := Name_Addr;
                  Tok.Name_L := Name_Len;

                  Tok.Value_A := Null_Address;
                  Tok.Value_L := 0;

                  Tok_Len := 4 + Name_Len + 1;
               end;

            when Tok_Prop =>
               declare
                  --  Type is followed by the value length and offset
                  --  of the name within the string table, and then
                  --  its value.
                  Value_Len : constant Storage_Count  :=
                    Storage_Count (Tok_Meta (2));
                  Name_Off  : constant Storage_Offset :=
                    Storage_Offset (Tok_Meta (3));

                  Name_Addr : constant Address       :=
                    St.Hdr.all'Address +
                    Storage_Offset (St.Hdr.Off_Dt_Strings) +
                    Name_Off;
                  Name_Len  : constant Storage_Count := Strlen (Name_Addr);
               begin
                  Tok.Name_A := Name_Addr;
                  Tok.Name_L := Name_Len;

                  Tok.Value_A := St.Addr + 12;
                  Tok.Value_L := Value_Len;

                  Tok_Len := 12 + Value_Len;
               end;

            when Tok_End_Node | Tok_Nop | Tok_End =>
               --  No associated data.
               Tok.Name_A := Null_Address;
               Tok.Name_L := 0;

               Tok.Value_A := Null_Address;
               Tok.Value_L := 0;

               Tok_Len := 4;
         end case;

         --  Tokens are aligned on a 4-byte boundary.
         if Tok_Len rem 4 /= 0 then
            Tok_Len := Tok_Len + (4 - Tok_Len rem 4);
         end if;
         St.Addr := St.Addr + Tok_Len;
      end Next;
   end Iter;
end Fdt;
