package body Capability.Table is
   function Guard
     (Cap        : aliased in out Slot;
      Guard      :                Pointer;
      Guard_Bits :                Pointer_Bits)
     return Errno.Typ is
   begin
      if Cap.Typ /= T.Table then
         return Errno.Invalid_Capability;
      end if;

      Cap.Ta_Guard      := Guard;
      Cap.Ta_Guard_Bits := Guard_Bits;

      return Errno.Success;
   end Guard;

   function Resolve_Pointer
     (Table : aliased in out Slot;
      Cap   :            out Slot_Access;
      Ptr   :                Pointer;
      Depth :                Natural)
     return Errno.Typ is
   begin
      if Depth = 0 then
         Cap := Table'Access;
         return Errno.Success;
      end if;

      if Table.Typ /= T.Table then
         return Errno.Invalid_Capability;
      end if;

      if not Table.Read then
         return Errno.Not_Authorized;
      end if;

      declare
         Caps : aliased Slot_Array (0 .. 2 ** Table.Ta_Table_Bits - 1);
         for Caps'Address use Table.Ta_Table;
         pragma Import (Ada, Caps);

         Table_Bits : constant Pointer_Bits :=
           Table.Ta_Table_Bits + Slot_Bits;

         Guard : constant Pointer :=
           Ptr / 2 ** (32 - Table.Ta_Guard_Bits);
         Index : constant Pointer :=
           (Ptr / 2 ** (32 - Table.Ta_Guard_Bits - Table_Bits)) and
           (2 ** Table_Bits - 1);

         Next : constant Pointer :=
           Ptr * 2 ** (Table.Ta_Guard_Bits + Table_Bits);
      begin
         if Guard /= Table.Ta_Guard then
            return Errno.Bad_Guard;
         end if;

         return Resolve_Pointer
           (Caps (Natural (Index)),
            Cap, Next, Depth - 1);
      end;
   end Resolve_Pointer;
end Capability.Table;
