--  Late initialization.

with Capability;
with Capability.Process;
with Capability.Scheduling_Context;
with Defs;
with System; use System;

package Init is
   --  Finish initializing everything and run the init executable.
   procedure Finish;
   pragma No_Return (Finish);

   --  Minimum bits for an untyped capability.
   Min_Untyped_Bits : constant Capability.Address_Bits := 23;

   --  Make untyped capabilities corresponding to usable memory.
   procedure Make_Untyped
     (Addr : Address;
      Bits : Capability.Address_Bits);

   Arch_Table_Cap : access Capability.Slot;

   Process_Cap  : access Capability.Slot;
   Process_Data : aliased Capability.Process.Process_Data;

   Table_Cap  : aliased Capability.Slot;
   Table_Data : aliased Capability.Slot_Array (0 .. 2 ** 6 - 1);

   Scheduler_Cap  : access Capability.Slot;
   Scheduler_Data : aliased Capability.Scheduling_Context.Scheduler_Data;

   Table_I : Natural := 3;
end Init;
pragma Preelaborate (Init);
