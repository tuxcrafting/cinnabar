--  System calls.

with Capability.Scheduling_Context;
with Syscall_Num;
with Types; use Types;

procedure Syscall
  (Num   :                Syscall_Num.Typ;
   Sched : aliased in out Capability.Scheduling_Context.Scheduler_Data);
pragma Preelaborate (Syscall);
