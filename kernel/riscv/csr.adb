with System.Machine_Code; use System.Machine_Code;

package body Csr is
   procedure Write
     (Num   : Csr_Num;
      Value : Word) is
   begin
      Asm
        ("csrw %0, %1",
         Inputs => (Integer'Asm_Input ("I", Csr_Num'Enum_Rep (Num)),
                    Word'Asm_Input ("Kr", Value)),
         Volatile => True);
   end Write;

   procedure Clear
     (Num  : Csr_Num;
      Mask : Word) is
   begin
      Asm
        ("csrc %0, %1",
         Inputs => (Integer'Asm_Input ("I", Csr_Num'Enum_Rep (Num)),
                    Word'Asm_Input ("Kr", Mask)),
         Volatile => True);
   end Clear;

   procedure Set
     (Num  : Csr_Num;
      Mask : Word) is
   begin
      Asm
        ("csrs %0, %1",
         Inputs => (Integer'Asm_Input ("I", Csr_Num'Enum_Rep (Num)),
                    Word'Asm_Input ("Kr", Mask)),
         Volatile => True);
   end Set;

   function Read
     (Num : Csr_Num)
     return Word is

      Value : Word;
   begin
      Asm
        ("csrr %0, %1",
         Outputs => Word'Asm_Output ("=r", Value),
         Inputs  => (Integer'Asm_Input ("I", Csr_Num'Enum_Rep (Num))),
         Volatile => True);
      return Value;
   end Read;

   function Read_Write
     (Num   : Csr_Num;
      Value : Word)
     return Word is

      Old_Value : Word;
   begin
      Asm
        ("csrrw %0, %1, %2",
         Outputs => Word'Asm_Output ("=r", Old_Value),
         Inputs  => (Integer'Asm_Input ("I", Csr_Num'Enum_Rep (Num)),
                     Word'Asm_Input ("Kr", Value)),
         Volatile => True);
      return Old_Value;
   end Read_Write;

   function Read_Clear
     (Num  : Csr_Num;
      Mask : Word)
     return Word is

      Old_Value : Word;
   begin
      Asm
        ("csrrc %0, %1, %2",
         Outputs => Word'Asm_Output ("=r", Old_Value),
         Inputs  => (Integer'Asm_Input ("I", Csr_Num'Enum_Rep (Num)),
                     Word'Asm_Input ("Kr", Mask)),
         Volatile => True);
      return Old_Value;
   end Read_Clear;

   function Read_Set
     (Num  : Csr_Num;
      Mask : Word)
     return Word is

      Old_Value : Word;
   begin
      Asm
        ("csrrs %0, %1, %2",
         Outputs => Word'Asm_Output ("=r", Old_Value),
         Inputs  => (Integer'Asm_Input ("I", Csr_Num'Enum_Rep (Num)),
                     Word'Asm_Input ("Kr", Mask)),
         Volatile => True);
      return Old_Value;
   end Read_Set;
end Csr;
