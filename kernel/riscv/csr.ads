--  RISC-V CSR access functions.

with Types; use Types;

package Csr is
   --  CSR addresses.
   type Csr_Num is
     (Fflags, Frm, Fcsr,
      Sstatus, Sedeleg, Sideleg, Sie,
      Stvec, Scounteren, Sscratch, Sepc,
      Scause, Stval, Sip, Satp,
      Time, Timeh);

   for Csr_Num use
     (Fflags     => 16#001#,
      Frm        => 16#002#,
      Fcsr       => 16#003#,
      Sstatus    => 16#100#,
      Sedeleg    => 16#102#,
      Sideleg    => 16#103#,
      Sie        => 16#104#,
      Stvec      => 16#105#,
      Scounteren => 16#106#,
      Sscratch   => 16#140#,
      Sepc       => 16#141#,
      Scause     => 16#142#,
      Stval      => 16#143#,
      Sip        => 16#144#,
      Satp       => 16#180#,
      Time       => 16#C01#,
      Timeh      => 16#C80#);

   --  Write a value to a CSR.
   procedure Write
     (Num   : Csr_Num;
      Value : Word);
   pragma Inline_Always (Write);

   --  Clear bits in a CSR set in the mask.
   procedure Clear
     (Num  : Csr_Num;
      Mask : Word);
   pragma Inline_Always (Clear);

   --  Set bits in a CSR set in the mask.
   procedure Set
     (Num  : Csr_Num;
      Mask : Word);
   pragma Inline_Always (Set);

   --  Read the value of a CSR.
   function Read
     (Num : Csr_Num)
     return Word;
   pragma Inline_Always (Read);

   --  Combined read/writes.

   function Read_Write
     (Num   : Csr_Num;
      Value : Word)
     return Word;
   pragma Inline_Always (Read_Write);

   function Read_Clear
     (Num  : Csr_Num;
      Mask : Word)
     return Word;
   pragma Inline_Always (Read_Clear);

   function Read_Set
     (Num  : Csr_Num;
      Mask : Word)
     return Word;
   pragma Inline_Always (Read_Set);
end Csr;
pragma Preelaborate (Csr);
