with Console; use Console;
with System;  use System;
with Types;   use Types;

package body Machine_State is
   procedure Print
     (St : aliased Msd.State) is

      procedure Put_Word is new Put_Unsigned (Num => Word);

      procedure P
        (Reg : String;
         Val : Word);

      procedure P
        (Reg : String;
         Val : Word) is
      begin
         Put (Reg & " = 16#");
         Put_Word (Val, Word_Size / 4, 16);
         Put_Line ("#");
      end P;
   begin
      P ("pc", St.Pc);
      P ("x1 / ra", St.Ra);
      P ("x2 / sp", St.Sp);
      P ("x3 / gp", St.Gp);
      P ("x4 / tp", St.Tp);
      P ("x5 / t0", St.T0);
      P ("x6 / t1", St.T1);
      P ("x7 / t2", St.T2);
      P ("x8 / fp", St.Fp);
      P ("x9 / s1", St.S1);
      P ("x10 / a0", St.A0);
      P ("x11 / a1", St.A1);
      P ("x12 / a2", St.A2);
      P ("x13 / a3", St.A3);
      P ("x14 / a4", St.A4);
      P ("x15 / a5", St.A5);
      P ("x16 / a6", St.A6);
      P ("x17 / a7", St.A7);
      P ("x18 / s2", St.S2);
      P ("x19 / s3", St.S3);
      P ("x20 / s4", St.S4);
      P ("x21 / s5", St.S5);
      P ("x22 / s6", St.S6);
      P ("x23 / s7", St.S7);
      P ("x24 / s8", St.S8);
      P ("x25 / s9", St.S9);
      P ("x26 / s10", St.S10);
      P ("x27 / s11", St.S11);
      P ("x28 / t3", St.T3);
      P ("x29 / t4", St.T4);
      P ("x30 / t5", St.T5);
      P ("x31 / t6", St.T6);
   end Print;

   procedure Switch
     (St : aliased Msd.State) is

      procedure C_Switch
        (Addr : Address);
      pragma No_Return (C_Switch);
      pragma Import (C, C_Switch, "_switch_to");
   begin
      C_Switch (St'Address);
   end Switch;
end Machine_State;
