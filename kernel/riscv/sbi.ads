--  RISC-V SBI v0.2 bindings.

package Sbi is
   --  SBI call return struct.
   type Sbi_Return is
      record
         Error, Value : Long_Integer;
      end record;

   --  Wrapper over ecall.
   function Ecall
     (A0, A1, A2, A3, A4, A5, A6, A7 : Long_Integer)
     return Sbi_Return;
   pragma Import (C, Ecall, "sbi_ecall");

   --  Put a character on the debug console.
   procedure Console_Putchar
     (Ch : Character);
end Sbi;
pragma Preelaborate (Sbi);
