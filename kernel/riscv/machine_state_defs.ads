with Types; use Types;

package Machine_State_Defs is
   type State is
      record
         Pc, Ra, Sp, Gp, Tp, T0, T1, T2,
           Fp, S1, A0, A1, A2, A3, A4, A5,
           A6, A7, S2, S3, S4, S5, S6, S7,
           S8, S9, S10, S11, T3, T4, T5, T6 : aliased Word;
      end record;
   pragma Pack (State);
end Machine_State_Defs;
pragma Preelaborate (Machine_State_Defs);
pragma Pure (Machine_State_Defs);
