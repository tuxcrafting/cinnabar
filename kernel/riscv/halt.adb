with Interrupts;
with System.Machine_Code; use System.Machine_Code;

procedure Halt is
begin
   Interrupts.Disable;

   loop
      Asm ("wfi", Volatile => True);
   end loop;
end Halt;
