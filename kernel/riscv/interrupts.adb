with Capability.Scheduling_Context;
with Console; use Console;
with Csr;
with Halt;
with Syscall;
with Syscall_Num;
with Types;   use Types;

package body Interrupts is
   procedure Init is

      procedure Isr;
      pragma Import (C, Isr, "_isr");
   begin
      --  Initialize Int_Ptr.
      Ptr0 :=
        (Null_Address, State0'Address,
         Stack0'Address + Stack0'Length, Null_Address);

      --  sscratch points to a per-processor Int_Ptr - currently this
      --  is just a static one.
      Csr.Write (Csr.Sscratch, Word (To_Integer (Ptr0'Address)));

      --  stvec is set to the ISR in direct mode.
      Csr.Write (Csr.Stvec, Word (To_Integer (Isr'Address)));
   end Init;

   procedure Handler
     (Ptr_Addr : Address) is

      Ptr : aliased constant Int_Ptr;
      for Ptr'Address use Ptr_Addr;
      pragma Import (Ada, Ptr);

      State : aliased Machine_State.Msd.State;
      for State'Address use Ptr.State_Addr;
      pragma Import (Ada, State);

      Sched_Data : aliased Capability.Scheduling_Context.Scheduler_Data;
      for Sched_Data'Address use Ptr.Scheduler;
      pragma Import (Ada, Sched_Data);

      Sstatus : constant Word := Csr.Read (Csr.Sstatus);
      Scause  : constant Word := Csr.Read (Csr.Scause);

      Scause_Interrupt : constant Boolean      :=
        Boolean'Val (Scause / 2 ** (Word_Size - 1));
      Scause_Except    : constant Long_Integer :=
        Long_Integer (Scause and 2 ** (Word_Size - 1) - 1);

      procedure Put_Word is new Put_Unsigned (Num => Word);
   begin
      --  If it's an user program, save the state.
      if (Sstatus and 2 ** 8) = 0 then
         Sched_Data.Current.State := State;
      end if;

      --  ecall
      if not Scause_Interrupt and Scause_Except = 8 then
         Sched_Data.Current.State.Pc := Sched_Data.Current.State.Pc + 4;
         Syscall (Syscall_Num.Typ'Val (State.A0), Sched_Data);
         Capability.Scheduling_Context.Continue (Sched_Data);
      end if;

      Put_Line ("Interrupted");

      Put ("sstatus = 2#");
      Put_Word (Sstatus, Word_Size, 2);
      Put_Line ("#");

      Put ("scause = 16#");
      Put_Word (Scause, Word_Size / 4, 16);
      Put ("# -- ");

      if Scause_Interrupt then
         case Scause_Except is
            when 1 =>
               Put_Line ("Software interrupt");
            when 5 =>
               Put_Line ("Timer interrupt");
            when 9 =>
               Put_Line ("External interrupt");
            when others =>
               Put_Line
                 ("Unknown interrupt " &
                    Long_Integer'Image (Scause_Except));
         end case;
      else
         case Scause_Except is
            when 0 =>
               Put_Line ("Instruction address misaligned");
            when 1 =>
               Put_Line ("Instruction access fault");
            when 2 =>
               Put_Line ("Illegal instruction");
            when 3 =>
               Put_Line ("Breakpoint");
               Halt;
            when 4 =>
               Put_Line ("Load address misaligned");
            when 5 =>
               Put_Line ("Load access fault");
            when 6 =>
               Put_Line ("Store/AMO address misaligned");
            when 7 =>
               Put_Line ("Store/AMO access fault");
            --  when 8 =>
            --     Put_Line ("Environment call from U-mode");
            when 9 =>
               Put_Line ("Environment call from S-mode");
            when 12 =>
               Put_Line ("Instruction page fault");
            when 13 =>
               Put_Line ("Load page fault");
            when 15 =>
               Put_Line ("Store/AMO page fault");
            when others =>
               Put_Line
                 ("Unknown exception " &
                    Long_Integer'Image (Scause_Except));
         end case;
      end if;

      Machine_State.Print (State);
      Halt;
   end Handler;

   procedure Enable is
   begin
      --  Enable sstatus.SIE.
      Csr.Set (Csr.Sstatus, 2#10#);

      --  Enable sie.SSIP, STIP, and SEIP.
      Csr.Set (Csr.Sie, 2#1000100010#);
   end Enable;

   procedure Disable is
   begin
      --  Disable sstatus.SIE.
      Csr.Clear (Csr.Sstatus, 2#10#);
   end Disable;
end Interrupts;
