--  RISC-V interrupt handling.

with Machine_State;
with System;                  use System;
with System.Storage_Elements; use System.Storage_Elements;

package Interrupts is
   --  Per-processor interrupt handling data.
   type Int_Ptr is
      record
         Tmp, State_Addr, Stack, Scheduler : aliased Address;
      end record;
   pragma Pack (Int_Ptr);

   State0 : aliased Machine_State.Msd.State;
   Stack0 : aliased Storage_Array (1 .. 4096);

   Ptr0 : aliased Int_Ptr;

   --  Set up interrupt handling.
   procedure Init;

   --  Procedure called by the ISR.
   procedure Handler
     (Ptr_Addr : Address);
   pragma No_Return (Handler);
   pragma Export (C, Handler, "int_handler");

   --  Enable interrupts.
   procedure Enable;

   --  Disable interrupts.
   procedure Disable;
end Interrupts;
pragma Preelaborate (Interrupts);
