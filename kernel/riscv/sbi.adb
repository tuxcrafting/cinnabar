package body Sbi is
   procedure Console_Putchar
     (Ch : Character) is

      Ignore : Sbi_Return;
   begin
      Ignore := Ecall
        (Long_Integer (Character'Pos (Ch)),
         0, 0, 0, 0, 0, 0,
         16#01#);
   end Console_Putchar;
end Sbi;
