#if BITS == 32
#define lr_ins "lw"
#define sr_ins "sw"
#define off(n) n * 4
#else
#define lr_ins "ld"
#define sr_ins "sd"
#define off(n) n * 8
#endif

#define strof_(x) #x
#define strof(x) strof_(x)

#define sr_(n, r) sr_ins " " #r ", " #n "(a1);"
#define sr(n, r) sr_(n, r)

#define lr_(n, r) lr_ins " " #r ", " #n "(a1);"
#define lr(n, r) lr_(n, r)

#define opr_regs(f) \
	f(off(1), ra) f(off(2), sp) f(off(3), gp) \
	f(off(4), tp) f(off(5), t0) f(off(6), t1) f(off(7), t2) \
	f(off(8), fp) f(off(9), s1) /*f(off(10), a0) f(off(11), a1)*/ \
	f(off(12), a2) f(off(13), a3) f(off(14), a4) f(off(15), a5) \
	f(off(16), a6) f(off(17), a7) f(off(18), s2) f(off(19), s3) \
	f(off(20), s4) f(off(21), s5) f(off(22), s6) f(off(23), s7) \
	f(off(24), s8) f(off(25), s9) f(off(26), s10) f(off(27), s11) \
	f(off(28), t3) f(off(29), t4) f(off(30), t5) f(off(31), t6)

#define save_regs opr_regs(sr)
#define load_regs opr_regs(lr)

void int_handler();
static volatile void(*t)() = &int_handler;

__attribute__((naked))
__attribute__((aligned(4)))
void _isr() {
	asm volatile(
		"csrrw a0, sscratch, a0;"
		sr_ins " a1, " strof(off(0)) "(a0);"
		lr_ins " a1, " strof(off(1)) "(a0);"
		save_regs
		"mv t0, a0;"
		"csrrw a0, sscratch, a0;"
		sr_ins " a0, " strof(off(10)) "(a1);"
		lr_ins " t1, " strof(off(0)) "(t0);"
		sr_ins " t1, " strof(off(11)) "(a1);"
		"mv a0, t0;"
		lr_ins " sp, " strof(off(2)) "(a0);"
		"csrr t0, sepc;"
		sr_ins " t0, " strof(off(0)) "(a1);");
	t();
}

void _switch_to(void* state) {
	asm volatile(
		"mv a1, %0;"
		lr_ins " t0, " strof(off(0)) "(a1);"
		"csrw sepc, t0;"
		load_regs
		lr_ins " a0, " strof(off(10)) "(a1);"
		lr_ins " a1, " strof(off(11)) "(a1);"
		"sret" :: "r"(state));
}
