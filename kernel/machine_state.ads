--  Machine register state.

with Machine_State_Defs;

package Machine_State is
   --  Only expected to provide the State record.
   package Msd renames Machine_State_Defs;

   --  Print a machine state's registers.
   procedure Print
     (St : aliased Msd.State);

   --  Context switch to a machine state.
   procedure Switch
     (St : aliased Msd.State);
end Machine_State;
pragma Preelaborate (Machine_State);
