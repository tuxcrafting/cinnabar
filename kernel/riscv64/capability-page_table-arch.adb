with Csr;
with System.Storage_Elements; use System.Storage_Elements;
with Types;                   use Types;
with Util;

package body Capability.Page_Table.Arch is
   type Rsw_Type is mod 2 ** 2;
   type Ppn_Type is mod 2 ** 44;
   type Pad_Type is mod 2 ** 10;

   type Table_Entry is
      record
         V, R, W, X, U, G, A, D : Boolean;
         Rsw                    : Rsw_Type;
         Ppn                    : Ppn_Type;
         Pad                    : Pad_Type;
      end record;

   for Table_Entry use
      record
         V   at 0 range  0 ..  0;
         R   at 0 range  1 ..  1;
         W   at 0 range  2 ..  2;
         X   at 0 range  3 ..  3;
         U   at 0 range  4 ..  4;
         G   at 0 range  5 ..  5;
         A   at 0 range  6 ..  6;
         D   at 0 range  7 ..  7;
         Rsw at 0 range  8 ..  9;
         Ppn at 0 range 10 .. 53;
         Pad at 0 range 54 .. 63;
      end record;
   for Table_Entry'Size use 64;

   type Table_Entry_Array is
     array (Natural range <>) of aliased Table_Entry;

   procedure Internal_Map
     (Table, Addr            : Address;
      V, R, W, X, U, G, A, D : Boolean;
      Index                  : Natural);
   procedure Internal_Map
     (Table, Addr            : Address;
      V, R, W, X, U, G, A, D : Boolean;
      Index                  : Natural) is

      Entries : aliased Table_Entry_Array (0 .. 511);
      for Entries'Address use Util.To_Virt (Table);
      pragma Import (Ada, Entries);
   begin
      Entries (Index) :=
        (V, R, W, X, U, G, A, D, 0,
         Ppn_Type (To_Integer (Addr) / 2 ** 12), 0);
   end Internal_Map;

   procedure Map
     (Table, Addr : Address;
      Typ         : Mapping_Type;
      Index       : Natural) is
   begin
      case Typ is
         when None =>
            Internal_Map
              (Table, Addr,
               False, False, False, False, False, False, False, False,
               Index);

         when Page_Table =>
            Internal_Map
              (Table, Addr,
               True, False, False, False, False, False, True, True,
               Index);

         when Page_R =>
            Internal_Map
              (Table, Addr,
               True, True, False, False, True, False, True, True,
               Index);

         when Page_Rw =>
            Internal_Map
              (Table, Addr,
               True, True, True, False, True, False, True, True,
               Index);

         when Page_Rx =>
            Internal_Map
              (Table, Addr,
               True, True, False, True, True, False, True, True,
               Index);

         when Page_W =>
            Internal_Map
              (Table, Addr,
               True, False, True, False, True, False, True, True,
               Index);
      end case;
   end Map;

   procedure Activate
     (Table : Address) is
   begin
      --  Sv48
      Csr.Write (Csr.Satp, Word (To_Integer (Table)) / 2 ** 12 + 9 * 2 ** 60);
   end Activate;

   procedure Init_Pt
     (Table : Address;
      Level : Natural) is
   begin
      if Level < 4 then
         return;
      end if;

      -- ID-map high half if level 4.
      for Index in 256 .. 511 loop
         Internal_Map
           (Table, To_Address (Integer_Address (Index) * 2 ** 39),
            True, True, True, True, False, True, True, True,
            Index);
      end loop;
   end Init_Pt;

   function Valid_Index
     (Level, Index : Natural)
     return Boolean is
   begin
      if Level = 4 then
         return Index in 0 .. 255;
      else
         return Index in 0 .. 511;
      end if;
   end Valid_Index;
end Capability.Page_Table.Arch;
