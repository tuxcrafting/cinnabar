with Csr;
with Machine_State;
with Types; use Types;

package body Capability.Process.Arch is
   procedure Switch
     (Process : aliased Process_Data) is
   begin
      --  SPIE = 1 (interrupts enabled), SPP = 0 (user mode)
      Csr.Set (Csr.Sstatus, 2 ** 5);
      Csr.Clear (Csr.Sstatus, 2 ** 8);
      Machine_State.Switch (Process.State);
   end Switch;
end Capability.Process.Arch;
