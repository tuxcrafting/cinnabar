with Capability;
with Capability.Base;
with Capability.Page_Table;
with Capability.Untyped;
with Errno;
with Init.Loader;
with Interrupts;
with Memory;
with System;                  use System;
with System.Storage_Elements; use System.Storage_Elements;
with Types;                   use Types;
with Util;

package body Init.Arch is
   Arch_Table_Data : aliased Capability.Slot_Array (0 .. 2 ** 3 - 1);

   procedure Init_Process is
      Page_Table_Data : aliased Storage_Array (0 .. 2 ** 13 * 4 - 1);
      pragma Import (C, Page_Table_Data, "data_4k");

      Ipc_Buf_Data : aliased Storage_Array (0 .. 2 ** 12 - 1);
      for Ipc_Buf_Data'Address use Page_Table_Data'Address + 2 ** 13 * 4;
      pragma Import (Ada, Ipc_Buf_Data);

      Page_Data : aliased Storage_Array (0 .. 2 ** 21 * 3 - 1);
      pragma Import (C, Page_Data, "data_2m");

      Pt4 : Capability.Slot renames Arch_Table_Data (0);
      Pt3 : Capability.Slot renames Arch_Table_Data (1);
      Pt2 : Capability.Slot renames Arch_Table_Data (2);
      Pt1 : Capability.Slot renames Arch_Table_Data (3);

      Page_Rx  : Capability.Slot renames Arch_Table_Data (4);
      Page_R   : Capability.Slot renames Arch_Table_Data (5);
      Page_Rw  : Capability.Slot renames Arch_Table_Data (6);
      Page_Ipc : Capability.Slot renames Arch_Table_Data (7);

      Exec : Loader.Executable;

      Untyped : aliased Capability.Slot;
      Ignore  : Errno.Typ;

      Ignore_Addr : Address;
   begin
      declare
         pragma Suppress (Discriminant_Check);
      begin
         Arch_Table_Cap.all :=
           (Capability.T.Table, False, True, True, False, null, null, null,
            Arch_Table_Data'Address, 0, 3, 0);
      end;

      Interrupts.Ptr0.Scheduler := Scheduler_Data'Address;

      --  Create page table and page capabilities.
      Capability.Untyped.Make
        (Untyped, Util.To_Phy (Page_Table_Data'Address), 15);
      Ignore := Capability.Untyped.Retype
        (Untyped, Arch_Table_Cap.all, Capability.T.Page_Table,
         0, 4, 13);
      Ignore := Capability.Base.Delete (Untyped);

      Capability.Untyped.Make
        (Untyped, Util.To_Phy (Page_Data'Address), 23);
      Ignore := Capability.Untyped.Retype
        (Untyped, Arch_Table_Cap.all, Capability.T.Page,
         4, 3, 21);
      Ignore := Capability.Base.Delete (Untyped);

      Capability.Untyped.Make
        (Untyped, Util.To_Phy (Ipc_Buf_Data'Address), 12);
      Ignore := Capability.Untyped.Retype
        (Untyped, Arch_Table_Cap.all, Capability.T.Page,
         7, 1, 12);
      Ignore := Capability.Base.Delete (Untyped);

      Ignore := Capability.Page_Table.Set_Level (Pt4, 4);
      Ignore := Capability.Page_Table.Set_Level (Pt3, 3);
      Ignore := Capability.Page_Table.Set_Level (Pt2, 2);
      Ignore := Capability.Page_Table.Set_Level (Pt1, 1);

      Ignore := Capability.Base.Clone (Pt4, Process_Data.Page_Table);
      Ignore := Capability.Base.Clone (Page_Ipc, Process_Data.Ipc_Buf);

      --  Map everything.
      --  - 0x001000-0x001FFF RW IPC buffer
      --  - 0x200000-0x3FFFFF RX .text
      --  - 0x400000-0x5FFFFF R .rodata
      --  - 0x600000-0x7FFFFF RW .data .bss stack
      Ignore := Capability.Page_Table.Map
        (Pt4, Pt3, Capability.Page_Table.Page_Table, 0);
      Ignore := Capability.Page_Table.Map
        (Pt3, Pt2, Capability.Page_Table.Page_Table, 0);
      Ignore := Capability.Page_Table.Map
        (Pt2, Pt1, Capability.Page_Table.Page_Table, 0);

      Ignore := Capability.Page_Table.Map
        (Pt1, Page_Ipc, Capability.Page_Table.Page_Rw, 1);

      Ignore := Capability.Page_Table.Map
        (Pt2, Page_Rx, Capability.Page_Table.Page_Rx, 1);
      Ignore := Capability.Page_Table.Map
        (Pt2, Page_R, Capability.Page_Table.Page_R, 2);
      Ignore := Capability.Page_Table.Map
        (Pt2, Page_Rw, Capability.Page_Table.Page_Rw, 3);

      --  Load init executable into memory.
      Loader.Load (Exec);
      Ignore_Addr := Memory.Memcpy
        (Util.To_Virt (Page_Rx.Pa_Addr), Exec.Rx_Addr,
         Storage_Count (Exec.Header_Access.Rx_Len), 0);
      Ignore_Addr := Memory.Memcpy
        (Util.To_Virt (Page_R.Pa_Addr), Exec.R_Addr,
         Storage_Count (Exec.Header_Access.R_Len), 0);
      Ignore_Addr := Memory.Memcpy
        (Util.To_Virt (Page_Rw.Pa_Addr), Exec.Rw_Addr,
         Storage_Count (Exec.Header_Access.Rw_Len), 0);

      --  Setup process state.
      Process_Data.State.Pc := Word (Exec.Header_Access.Entry_Point);
      Process_Data.State.Sp := 16#800000#;
   end Init_Process;
end Init.Arch;
