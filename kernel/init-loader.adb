with System.Storage_Elements; use System.Storage_Elements;

package body Init.Loader is
   procedure Load
     (Exec : out Executable) is

      Hdr : aliased Header;
      pragma Import (C, Hdr, "_binary_init_bin_start");
   begin
      Exec.Header_Access := Hdr'Unchecked_Access;
      Exec.Rx_Addr       := Hdr'Address + 20;
      Exec.R_Addr        := Exec.Rx_Addr + Storage_Count (Hdr.Rx_Len);
      Exec.Rw_Addr       := Exec.R_Addr + Storage_Count (Hdr.R_Len);
   end Load;
end Init.Loader;
