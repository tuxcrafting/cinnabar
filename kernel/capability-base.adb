with Capability.Page_Table;
with Defs;

package body Capability.Base is
   function Same_Obj
     (A, B : aliased Slot)
     return Boolean is
   begin
      if A.Typ /= B.Typ then
         return False;
      end if;

      case A.Typ is
         when T.Nil =>
            return False;
         when T.Table =>
            return A.Ta_Table = B.Ta_Table;
         when T.Page_Table =>
            return A.Pt_Addr = B.Pt_Addr;
         when others =>
            return False;
      end case;
   end Same_Obj;

   procedure Add_Child
     (Cap, New_Child : aliased in out Slot) is
   begin
      --  I have to use 'Unchecked_Access in this function. I'm not
      --  sure why, but GNAT won't accept a plain 'Access.

      --  If the parent capability already has children, unset the
      --  first child's Fst_Child property and make its previous
      --  capability the new child.
      if Cap.Child /= null then
         Cap.Child.Fst_Child := False;
         Cap.Child.Prev      := New_Child'Unchecked_Access;
      end if;

      --  New child is the first child, previous capability is the
      --  parent capability, next capability is the old first child
      --  (which may be null).
      New_Child.Fst_Child := True;
      New_Child.Prev      := Cap'Unchecked_Access;
      New_Child.Next      := Cap.Child;

      --  Finally, add the child to the parent capability.
      Cap.Child := New_Child'Unchecked_Access;
   end Add_Child;

   function Remint
     (Cap         : aliased in out Slot;
      Read, Write :                Boolean)
     return Errno.Typ is
   begin
      if Cap.Typ = T.Nil then
         return Errno.Nil_Capability;
      end if;

      Cap.Read  := Cap.Read and Read;
      Cap.Write := Cap.Write and Write;

      declare
         Child : access Slot := Cap.Child;
         Err   : Errno.Typ;
      begin
         while Child /= null loop
            --  Skip recursing if the child capability already has the
            --  same permissions.
            if Child.Read = Cap.Read and then Child.Write = Cap.Write then
               goto Next;
            end if;

            Err := Remint (Child.all, Read, Write);

            if Err /= Errno.Success then
               return Err;
            end if;

            <<Next>>
            Child := Child.Next;
         end loop;
      end;

      return Errno.Success;
   end Remint;

   function Clone
     (Cap, Dest : aliased in out Slot)
     return Errno.Typ is
   begin
      if Cap.Typ = T.Nil then
         return Errno.Nil_Capability;
      end if;

      if Dest.Typ /= T.Nil then
         return Errno.Non_Nil_Capability;
      end if;

      if Cap.Typ = T.Untyped and then Cap.Child /= null then
         return Errno.Final;
      end if;

      declare
         pragma Suppress (Discriminant_Check);
      begin
         Dest := Cap;
      end;

      Dest.Child := null;
      Add_Child (Cap, Dest);

      --  Page and page tables do not keep their mapped status.
      if Cap.Typ = T.Page then
         Dest.Pa_Table       := Null_Address;
         Dest.Pa_Table_Index := Pt_Index_Unmapped;
      end if;
      if Cap.Typ = T.Page_Table then
         Dest.Pt_Table       := Null_Address;
         Dest.Pt_Table_Index := Pt_Index_Unmapped;
      end if;

      return Errno.Success;
   end Clone;

   function Move
     (Cap, Dest : aliased in out Slot)
     return Errno.Typ is
   begin
      if Cap.Typ = T.Nil then
         return Errno.Nil_Capability;
      end if;

      if Dest.Typ /= T.Nil then
         return Errno.Non_Nil_Capability;
      end if;

      declare
         pragma Suppress (Discriminant_Check);
      begin
         Dest := Cap;
         Cap  := Empty_Slot;
      end;

      --  If the new capability has a previous capability, set the
      --  previous capability's next or child attribute to the new
      --  capability.
      if Dest.Prev /= null then
         if Dest.Fst_Child then
            Dest.Prev.Child := Dest'Access;
         else
            Dest.Prev.Next := Dest'Access;
         end if;
      end if;

      --  Similarly, relink the next capability.
      if Dest.Next /= null then
         Dest.Next.Prev := Dest'Access;
      end if;

      --  And the first child capability.
      if Dest.Child /= null then
         Dest.Child.Prev := Dest'Access;
      end if;

      --  Update the pointers in the mapping page table for pages and
      --  page tables.
      if Dest.Typ = T.Page
        and then Dest.Pa_Table_Index /= Pt_Index_Unmapped
      then
         Page_Table.Update_Addr
           (Dest.Pa_Table, Dest'Address, Dest.Pa_Table_Index);
      end if;
      if Dest.Typ = T.Page_Table
        and then Dest.Pt_Table_Index /= Pt_Index_Unmapped
      then
         Page_Table.Update_Addr
           (Dest.Pt_Table, Dest'Address, Dest.Pt_Table_Index);
      end if;

      return Errno.Success;
   end Move;

   function Delete
     (Cap : aliased in out Slot)
     return Errno.Typ is

      Err : Errno.Typ;
   begin
      if Cap.Typ = T.Nil then
         return Errno.Nil_Capability;
      end if;

      --  Check for current deletion.
      if Cap.Deleting then
         return Errno.Success;
      end if;
      Cap.Deleting := True;

      --  Relink the previous capability.
      if Cap.Prev /= null then
         if Cap.Fst_Child then
            Cap.Prev.Child := Cap.Next;
         else
            Cap.Prev.Next := Cap.Next;
         end if;
      end if;

      --  And the next one.
      if Cap.Next /= null then
         Cap.Next.Fst_Child := Cap.Fst_Child;
         Cap.Next.Prev      := Cap.Prev;
      end if;

      --  Now for children, the strategy is to insert them in the
      --  place of where the capability was. Now even if the
      --  capability didn't have a parent, it's important to conserve
      --  the linked list structure and order, for reference counting
      --  purposes.
      if Cap.Child /= null then
         declare
            --  First, find the first and last child (they may be the same).
            First_Child : constant access Slot := Cap.Child;
            Last_Child  : access Slot          := Cap.Child;
         begin
            while Last_Child.Next /= null loop
               Last_Child := Last_Child.Next;
            end loop;

            --  And, do the same as before, relink previous and next.
            if Cap.Prev /= null then
               if Cap.Fst_Child then
                  Cap.Prev.Child := First_Child;
               else
                  Cap.Prev.Next := First_Child;
               end if;
            end if;
            First_Child.Fst_Child := Cap.Fst_Child;
            First_Child.Prev      := Cap.Prev;

            if Cap.Next /= null then
               Cap.Next.Fst_Child := False;
               Cap.Next.Prev      := Last_Child;
            end if;
            Last_Child.Next := Cap.Next;
         end;
      end if;

      --  For pages and page tables, unmap if mapped.
      if Cap.Typ = T.Page
        and then Cap.Pa_Table_Index = Pt_Index_Unmapped
      then
         Page_Table.Unmap_Addr (Cap.Pa_Table, Cap.Pa_Table_Index);
      end if;
      if Cap.Typ = T.Page_Table
        and then Cap.Pt_Table_Index = Pt_Index_Unmapped
      then
         Page_Table.Unmap_Addr (Cap.Pt_Table, Cap.Pt_Table_Index);
      end if;

      --  Now, reference counting - if all the neighbors and first
      --  child of the deleted capability are either null or not
      --  referring to the same object, then it's safe to assume this
      --  is the last reference and the object can thus be destroyed.
      if
        (Cap.Prev = null or else not Same_Obj (Cap.Prev.all, Cap))
          and then
          (Cap.Next = null or else not Same_Obj (Cap.Next.all, Cap))
          and then
          (Cap.Child = null or else not Same_Obj (Cap.Child.all, Cap))
      then
         case Cap.Typ is
            when T.Table =>
               --  For Table capabilities, delete every capability
               --  within it.
               declare
                  Caps : aliased Slot_Array (1 .. 2 ** Cap.Ta_Table_Bits);
                  for Caps'Address use Cap.Ta_Table;
                  pragma Import (Ada, Caps);
               begin
                  for Caps_I in Caps'Range loop
                     if Caps (Caps_I).Typ /= T.Nil then
                        Err := Delete (Caps (Caps_I));

                        if Err /= Errno.Success then
                           goto End_Func;
                        end if;
                     end if;
                  end loop;
               end;

            when T.Page_Table =>
               --  For Page_Table capabilities, unmap every Page and
               --  Page_Table mapped to it.
               for Index in 0 .. Defs.Page_Table_Length - 1 loop
                  Page_Table.Unmap_Addr (Cap.Pt_Addr, Index);
               end loop;

            when others =>
               null;
         end case;
      end if;

      Err := Errno.Success;

      <<End_Func>>
      declare
         pragma Suppress (Discriminant_Check);
      begin
         Cap := Empty_Slot;
      end;

      return Errno.Success;
   end Delete;

   function Revoke
     (Cap : aliased in out Slot)
     return Errno.Typ is

      Err : Errno.Typ;
   begin
      if Cap.Typ = T.Nil then
         return Errno.Nil_Capability;
      end if;

      while Cap.Child /= null loop
         Err := Delete (Cap.Child.all);

         if Err /= Errno.Success then
            return Err;
         end if;
      end loop;

      return Errno.Success;
   end Revoke;
end Capability.Base;
