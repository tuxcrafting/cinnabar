--  Board initialization functions.

package Board_Init is
   --  Early board initialization. Does stuff like initializing
   --  output.
   procedure Early;

   --  Late board initialization. Probes the hardware and other things
   --  necessary before handing control over to userspace.
   procedure Late;
end Board_Init;
pragma Preelaborate (Board_Init);
