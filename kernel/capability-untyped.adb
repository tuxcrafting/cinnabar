with Capability.Base;
with Defs;
with Memory; use Memory;
with Types;  use Types;
with Util;

package body Capability.Untyped is
   procedure Make
     (Cap  : aliased in out Slot;
      Addr :                Address;
      Bits :                Address_Bits) is
   begin
      Cap :=
        (T.Untyped, False, True, True, False,
         null, null, null,
         Addr, Bits);
   end Make;

   function Retype
     (Cap       : aliased in out Slot;
      Table     : aliased        Slot;
      Typ       :                T.Typ;
      From, Num :                Natural;
      Bits      :                Address_Bits)
     return Errno.Typ is

      Ignore : Address;

      --  Cached page level from the bit length checks.
      Page_Level : Natural;
   begin
      --  Allll the checks.

      --  Check for any nil.
      if Cap.Typ /= T.Untyped or else
        Table.Typ /= T.Table
      then
         return Errno.Invalid_Capability;
      end if;

      --  Check for permissions.
      if not Table.Write then
         return Errno.Not_Authorized;
      end if;

      --  Check for final.
      if Cap.Child /= null then
         return Errno.Final;
      end if;

      declare
         Caps : aliased Slot_Array (0 .. 2 ** Table.Ta_Table_Bits - 1);
         for Caps'Address use Table.Ta_Table;
         pragma Import (Ada, Caps);
      begin
         --  Check for bounds.
         if Num = 0 or else From + Num - 1 > Caps'Last then
            return Errno.Bounds;
         end if;

         --  Check if the bits fit in the Cap_Untyped.
         if Word (Num) * 2 ** Bits > 2 ** Cap.Un_Bits then
            return Errno.Invalid_Size;
         end if;

         --  Check if the bits are valid for the specified type, and if
         --  that specified type is valid.
         case Typ is
            when T.Untyped =>
               null;

            when T.Table =>
               if Bits < Slot_Bits or else Bits > Max_Table_Bits then
                  return Errno.Invalid_Size;
               end if;

            when T.Page =>
               for Level_I in Defs.Page_Level_Bits'Range loop
                  if Bits = Defs.Page_Level_Bits (Level_I) then
                     Page_Level := Level_I - Defs.Page_Level_Bits'First;
                     goto Found_Page_Level;
                  end if;
               end loop;

               return Errno.Invalid_Size;

               <<Found_Page_Level>>

            when T.Page_Table =>
               if Bits /= Defs.Page_Table_Bits + 1 then
                  return Errno.Invalid_Size;
               end if;

            when T.Process =>
               if Bits /= Process_Bits then
                  return Errno.Invalid_Size;
               end if;

            when others =>
               return Errno.Invalid_Type;
         end case;

         --  Check for any non-nil.
         for Caps_I in From .. From + Num - 1 loop
            if Caps (Caps_I).Typ /= T.Nil then
               return Errno.Non_Nil_Capability;
            end if;
         end loop;

         --  Now, for the actual derivation.
         declare
            Cur_Addr : Address := Cap.Un_Addr;
         begin
            for Caps_I in From .. From + Num - 1 loop
               declare
                  Derived_Cap : Slot renames Caps (Caps_I);
               begin
                  case Typ is
                     when T.Untyped =>
                        Derived_Cap :=
                          (T.Untyped, False, False, False, False,
                           null, null, null,
                           Cur_Addr, Bits);

                     when T.Table =>
                        Derived_Cap :=
                          (T.Table, False, False, False, False,
                           null, null, null,
                           Util.To_Virt (Cur_Addr), 0, Bits - Slot_Bits, 0);

                     when T.Page =>
                        Derived_Cap :=
                          (T.Page, False, False, False, False,
                           null, null, null,
                           Cur_Addr, Null_Address,
                           Page_Level, Pt_Index_Unmapped);

                     when T.Page_Table =>
                        Derived_Cap :=
                          (T.Page_Table, False, False, False, False,
                           null, null, null,
                           Cur_Addr, Null_Address,
                           Pt_Level_Undef, Pt_Index_Unmapped);

                     when T.Process =>
                        Derived_Cap :=
                          (T.Process, False, False, False, False,
                           null, null, null,
                           Util.To_Virt (Cur_Addr));

                     when others =>
                        --  This should never happen.
                        null;
                  end case;

                  --  If the capability is not Untyped, clear its data.
                  if Typ /= T.Untyped then
                     Ignore :=
                       Memset (Util.To_Virt (Cur_Addr), 0, 2 ** Bits);
                  end if;

                  Derived_Cap.Read  := Cap.Read;
                  Derived_Cap.Write := Cap.Write;
                  Derived_Cap.Child := null;

                  Base.Add_Child (Cap, Derived_Cap);
               end;

               Cur_Addr := Cur_Addr + 2 ** Bits;
            end loop;
         end;
      end;

      return Errno.Success;
   end Retype;
end Capability.Untyped;
