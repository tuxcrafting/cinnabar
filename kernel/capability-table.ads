--  Table capability functions.

with Errno;

package Capability.Table is
   --  Change the guard of a Table capability.
   --
   --  - Invalid_Capability - Cap is not a Table capability.
   function Guard
     (Cap        : aliased in out Slot;
      Guard      :                Pointer;
      Guard_Bits :                Pointer_Bits)
     return Errno.Typ;

   --  Resolve a pointer Ptr within Table into Cap with a maximum
   --  depth of Depth.
   --
   --  - Invalid_Capability - One of the capabilities that is
   --    addressed is not Cap_Table.
   --  - Not_Authorized - A Cap_Table is not readable.
   --  - Bad_Guard - A guard in the pointer is invalid.
   function Resolve_Pointer
     (Table : aliased in out Slot;
      Cap   :            out Slot_Access;
      Ptr   :                Pointer;
      Depth :                Natural)
     return Errno.Typ;
end Capability.Table;
pragma Preelaborate (Capability.Table);
