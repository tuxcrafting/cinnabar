--  Architecture-specific part of the initialization.

package Init.Arch is
   --  Initialize the process and architecture-specific capability
   --  table.
   procedure Init_Process;
end Init.Arch;
pragma Preelaborate (Init.Arch);
