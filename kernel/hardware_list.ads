--  Platform-independent hardware specification.

with System;                  use System;
with System.Storage_Elements; use System.Storage_Elements;

package Hardware_List is
   --  Range of memory.
   type Memory_Range is
      record
         Start : Address;
         Size  : Storage_Count;
      end record;

   --  Available and reserved memory ranges.
   Available_Memory, Reserved_Memory     : array (1 .. 32) of Memory_Range;
   Available_Memory_N, Reserved_Memory_N : Natural := 0;

   --  Add an available range of memory.
   procedure Add_Available
     (Start : Address;
      Size  : Storage_Count);

   --  Add a reserved range of memory.
   procedure Add_Reserved
     (Start : Address;
      Size  : Storage_Count);
end Hardware_List;
pragma Preelaborate (Hardware_List);
