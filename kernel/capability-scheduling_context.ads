--  Scheduling context functions.

with Capability.Process;
with Errno;

package Capability.Scheduling_Context is
   type Process_Access is access all Process.Process_Data;
   type Process_Queue is
      record
         First, Last : Process_Access;
      end record;
   type Process_Queues is
     array (Natural range <>) of aliased Process_Queue;

   type Scheduler_Data is
      record
         Current : Process_Access;
         Queues  : aliased Process_Queues (0 .. Scheduler_Max_Prio);
      end record;

   --  Enqueue a process into a queue.
   procedure Enqueue
     (Queue : aliased in out Process_Queue;
      Proc  :                Process_Access);

   --  Dequeue a process from a queue.
   function Dequeue
     (Queue : aliased in out Process_Queue)
     return Process_Access;

   --  Remove a process from a queue.
   procedure Queue_Remove
     (Queue : aliased in out Process_Queue;
      Proc  :                Process_Access);

   --  Continue scheduling the current process, or reschedule if it's
   --  no longer continuing. Returns only if no process is available.
   procedure Continue
     (Sched : aliased in out Scheduler_Data);

   --  Schedule the next available process. Returns only if no process
   --  is available.
   procedure Next
     (Sched : aliased in out Scheduler_Data);
end Capability.Scheduling_Context;
pragma Preelaborate (Capability.Scheduling_Context);
