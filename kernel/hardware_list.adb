package body Hardware_List is
   procedure Add_Available
     (Start : Address;
      Size  : Storage_Count) is
   begin
      Available_Memory_N                    := Available_Memory_N + 1;
      Available_Memory (Available_Memory_N) := (Start, Size);
   end Add_Available;

   procedure Add_Reserved
     (Start : Address;
      Size  : Storage_Count) is
   begin
      Reserved_Memory_N                   := Reserved_Memory_N + 1;
      Reserved_Memory (Reserved_Memory_N) := (Start, Size);
   end Add_Reserved;
end Hardware_List;
