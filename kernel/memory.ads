--  Memory-related helper functions.

with System;                  use System;
with System.Storage_Elements; use System.Storage_Elements;

package Memory is
   --  Clear an area of memory with a given value.
   function Memset
     (Addr : Address;
      Ch   : Integer;
      Size : Storage_Count)
     return Address;
   pragma Export (C, Memset, "ada_memset");

   --  Copy an area of memory to another, in a specified direction.
   function Memcpy
     (Dest, Src : Address;
      Size      : Storage_Count;
      Revers    : Integer)
     return Address;
   pragma Export (C, Memcpy, "ada_memcpy");

   --  Compare two areas of memory.
   function Memcmp
     (A, B : Address;
      Size : Storage_Count)
     return Integer;
   pragma Export (C, Memcmp, "memcmp");

   --  Get the length of a C string.
   function Strlen
     (Str : Address)
     return Storage_Count;
   pragma Export (C, Strlen, "strlen");

   --  Zero out .bss.
   procedure Zero_Bss;
   pragma Export (C, Zero_Bss, "zero_bss");

   --  Get the kernel start and end addresses.
   procedure Get_Kernel_Start_End
     (Start_Addr, End_Addr : out Address);
end Memory;
pragma Preelaborate (Memory);
pragma Pure (Memory);
