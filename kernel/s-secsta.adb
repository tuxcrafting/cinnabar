with System; use System;
with System.Parameters; use System.Parameters;
with System.Storage_Elements; use System.Storage_Elements;

package body System.Secondary_Stack is
   procedure Ss_Allocate
     (Addr : out Address;
      Size :     Storage_Count) is
   begin
      Addr := Stack_Data (Storage_Offset (Stack_Pointer))'Address;
      Stack_Pointer := Stack_Pointer + Mark_Id (Size);
   end Ss_Allocate;

   procedure Ss_Free
     (Stack : in out Address) is
   begin
      null;
   end Ss_Free;

   function Ss_Mark
     return Mark_Id is
   begin
      return Stack_Pointer;
   end Ss_Mark;

   procedure Ss_Release
     (Mark : Mark_Id) is
   begin
      Stack_Pointer := Mark;
   end Ss_Release;
end System.Secondary_Stack;
