with Capability.Page_Table;
with Capability.Process.Arch;
with Capability.Scheduling_Context;

package body Capability.Process is
   procedure Set_Scheduling_Context
     (Process   : aliased in out Process_Data;
      Sched_Ctx :         access Scheduling_Context.Scheduler_Data;
      Prio      :                Natural) is
   begin
      if Process.Queued then
         Scheduling_Context.Queue_Remove
           (Process.Sched_Ctx.Queues (Process.Prio),
            Process'Unchecked_Access);
         Process.Queued := False;
      elsif Process.Current then
         Process.Sched_Ctx.Current := null;
         Process.Current := False;
      end if;
      Process.Sched_Ctx := Sched_Ctx;
      Process.Prio := Prio;
      Recheck (Process);
   end Set_Scheduling_Context;

   procedure Recheck
     (Process : aliased in out Process_Data) is
   begin
      if Process.Ipc_Buf.Typ = T.Page
        and then Process.Table.Typ = T.Table
        and then Process.Page_Table.Typ = T.Page_Table
        and then Process.Sched_Ctx /= null
        and then Process.Running
        and then not Process.Waiting
      then
         if not Process.Queued and then not Process.Current then
            Scheduling_Context.Enqueue
              (Process.Sched_Ctx.Queues (Process.Prio),
               Process'Unchecked_Access);
            Process.Queued := True;
         end if;
      elsif Process.Queued then
         Scheduling_Context.Queue_Remove
           (Process.Sched_Ctx.Queues (Process.Prio),
            Process'Unchecked_Access);
         Process.Queued := False;
      elsif Process.Current then
         Process.Sched_Ctx.Current := null;
         Process.Current := False;
      end if;
   end Recheck;

   procedure Switch
     (Process : aliased Process_Data) is
   begin
      Page_Table.Activate (Process.Page_Table.Pt_Addr);
      Arch.Switch (Process);
   end Switch;
end Capability.Process;
