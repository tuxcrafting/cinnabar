--  Return values and error codes.

package Errno is
   type Typ is (ERRNO);
end Errno;
pragma Preelaborate (Errno);
pragma Pure (Errno);
