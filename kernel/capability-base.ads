--  Base capability functions.

with Errno;

package Capability.Base is
   --  Check if two capabilities refer to the same underlying object.
   function Same_Obj
     (A, B : aliased Slot)
     return Boolean;

   --  Add a child to a capability.
   procedure Add_Child
     (Cap, New_Child : aliased in out Slot);

   --  Basic capability calls.

   --  Remint a capability - change its read/write permissions. A
   --  permission can be removed but not added, and any permission
   --  that would be added is silently ignored. This operation is
   --  recursive.
   --
   --  - Nil_Capability - Cap is nil.
   function Remint
     (Cap         : aliased in out Slot;
      Read, Write :                Boolean)
     return Errno.Typ;

   --  Make a children of a capability in another slot.
   --
   --  - Nil_Capability - Cap is nil.
   --  - Non_Nil_Capability - Dest is not nil.
   --  - Final - Cap is Untyped and already has children.
   function Clone
     (Cap, Dest : aliased in out Slot)
     return Errno.Typ;

   --  Move a capability from one slot to another.
   --
   --  - Nil_Capability - Cap is nil.
   --  - Non_Nil_Capability - Dest is not nil.
   function Move
     (Cap, Dest : aliased in out Slot)
     return Errno.Typ;

   --  Delete a capability from a slot.
   --
   --  - Nil_Capability - Cap is nil.
   function Delete
     (Cap : aliased in out Slot)
     return Errno.Typ;

   --  Delete every child capability of a capability, recursively.
   --
   --  - Nil_Capability - Cap is nil.
   function Revoke
     (Cap : aliased in out Slot)
     return Errno.Typ;
end Capability.Base;
pragma Preelaborate (Capability.Base);
