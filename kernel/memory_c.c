#include <stddef.h>

void* ada_memset(void*, int, size_t);
void* ada_memcpy(void*, const void*, size_t, int);

#define fn \
	__attribute__((used)) \
	__attribute__((__optimize__("-fno-tree-loop-distribute-patterns")))

fn void* memset(void* p, int c, size_t n) {
	return ada_memset(p, c, n);
}

fn void* memcpy(void* d, const void* s, size_t n) {
	return ada_memcpy(d, s, n, 0);
}

fn void* memcpy_r(void* d, const void* s, size_t n) {
	return ada_memcpy(d, s, n, 1);
}

fn void* memmove(void* d, const void* s, size_t n) {
	if (d < s) {
		return memcpy(d, s, n);
	} else {
		return memcpy_r(d, s, n);
	}
}

fn void i_hate_gcc() { }
