with Halt;
with Memory; use Memory;

package body Console is
   procedure Put
     (Char : Character) is
   begin
      if Board_Console_Put /= null then
         Board_Console_Put (Char);
      end if;
   end Put;

   procedure Put
     (Str : String) is
   begin
      for I in Str'Range loop
         Put (Str (I));
      end loop;
   end Put;

   procedure New_Line is
   begin
      Put (Character'Val (10));
   end New_Line;

   procedure Put_Line
     (Str : String) is
   begin
      Put (Str);
      New_Line;
   end Put_Line;

   procedure Put_Unsigned
     (N     : Num;
      Width : Natural;
      Base  : Int_Base := 10) is
   begin
      for I in reverse 1 .. Width loop
         declare
            Digit : constant Num := N / Num (Base) ** (I - 1) rem Num (Base);
         begin
            Put (Alphabet (Natural (Digit) + 1));
         end;
      end loop;
   end Put_Unsigned;

   procedure Panic is
   begin
      Put_Line ("Kernel panic!");
      Halt;
   end Panic;

   procedure Last_Chance_Handler
     (Source_Location : Address;
      Line            : Integer) is

      Source : String (1 .. Natural (Strlen (Source_Location)));
      for Source'Address use Source_Location;
      pragma Import (Ada, Source);
   begin
      Put_Line
        ("Uncaught error at " & Source &
           " line" & Integer'Image (Line));
      Panic;
   end Last_Chance_Handler;
end Console;
