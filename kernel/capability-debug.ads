--  Capability-related debugging functions.

package Capability.Debug is
   --  Print a capability's derivation tree.
   procedure Print
     (Cap   : aliased Slot;
      Level :         Natural := 0);
end Capability.Debug;
pragma Preelaborate (Capability.Debug);
