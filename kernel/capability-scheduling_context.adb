package body Capability.Scheduling_Context is
   procedure Enqueue
     (Queue : aliased in out Process_Queue;
      Proc  :                Process_Access) is
   begin
      if Queue.First = null then
         Queue.First := Proc;
         Queue.Last := Proc;
         Proc.Next := null;
         Proc.Prev := null;
      else
         Proc.Prev := Queue.Last;
         Proc.Prev.Next := Proc;
         Queue.Last := Proc;
         Proc.Next := null;
      end if;
   end Enqueue;

   function Dequeue
     (Queue : aliased in out Process_Queue)
     return Process_Access is
   begin
      if Queue.First = null then
         return null;
      else
         declare
            P : constant Process_Access := Queue.First;
         begin
            Queue.First := Queue.First.Next;
            if Queue.First = null then
               Queue.Last := null;
            else
               Queue.First.Prev := null;
            end if;
            return P;
         end;
      end if;
   end Dequeue;

   procedure Queue_Remove
     (Queue : aliased in out Process_Queue;
      Proc  :                Process_Access) is
   begin
      if Queue.First = Proc then
         Queue.First := Proc.Next;
      end if;
      if Queue.Last = Proc then
         Queue.Last := Proc.Prev;
      end if;
      if Proc.Prev /= null then
         Proc.Prev.Next := Proc.Next;
      end if;
      if Proc.Next /= null then
         Proc.Next.Prev := Proc.Prev;
      end if;
   end Queue_Remove;

   procedure Continue
     (Sched : aliased in out Scheduler_Data) is
   begin
      if Sched.Current /= null then
         Process.Switch (Sched.Current.all);
      end if;
      Next (Sched);
   end Continue;

   procedure Next
     (Sched : aliased in out Scheduler_Data) is
   begin
      for Prio in reverse 0 .. Scheduler_Max_Prio loop
         if Sched.Queues (Prio).First /= null then
            Sched.Current := Dequeue (Sched.Queues (Prio));
            Sched.Current.Queued := False;
            Sched.Current.Current := True;
            Process.Recheck (Sched.Current.all);
            if Sched.Current = null then
               Next (Sched);
               return;
            end if;
            Process.Switch (Sched.Current.all);
         end if;
      end loop;
   end Next;
end Capability.Scheduling_Context;
