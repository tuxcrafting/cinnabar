--  Flattened Device Tree functions.

with Interfaces;              use Interfaces;
with System;                  use System;
with System.Storage_Elements; use System.Storage_Elements;

package Fdt is
   --  FDT header magic number.
   Magic_Number : constant Unsigned_32 := 16#D00DFEED#;

   --  FDT version with which this package is compatible.
   Compatible_Version : constant Unsigned_32 := 17;

   --  FDT header.
   type Header is
      record
         Magic,
           Total_Size,
           Off_Dt_Struct,
           Off_Dt_Strings,
           Off_Mem_Rsvmap,
           Version,
           Last_Comp_Version,
           Boot_Cpuid_Phys,
           Size_Dt_Strings,
           Size_Dt_Structs : Unsigned_32;
      end record;

   for Header use
      record
         Magic             at  0 range 0 .. 31;
         Total_Size        at  4 range 0 .. 31;
         Off_Dt_Struct     at  8 range 0 .. 31;
         Off_Dt_Strings    at 12 range 0 .. 31;
         Off_Mem_Rsvmap    at 16 range 0 .. 31;
         Version           at 20 range 0 .. 31;
         Last_Comp_Version at 24 range 0 .. 31;
         Boot_Cpuid_Phys   at 28 range 0 .. 31;
         Size_Dt_Strings   at 32 range 0 .. 31;
         Size_Dt_Structs   at 36 range 0 .. 31;
      end record;

   for Header'Bit_Order use High_Order_First;
   for Header'Scalar_Storage_Order use High_Order_First;

   --  FDT token types.
   type Token_Type is
     (Tok_Begin_Node, Tok_End_Node, Tok_Prop, Tok_Nop, Tok_End);

   for Token_Type use
     (Tok_Begin_Node => 16#00000001#,
      Tok_End_Node   => 16#00000002#,
      Tok_Prop       => 16#00000003#,
      Tok_Nop        => 16#00000004#,
      Tok_End        => 16#00000009#);

   --  FDT token.
   type Token is
      record
         Typ             : Token_Type;
         --  Stored as address-length pairs instead of arrays due to
         --  Ada limitations.
         Name_A, Value_A : Address;
         Name_L, Value_L : Storage_Count;
      end record;

   --  Array of big-endian Unsigned_32's.
   type U32_Array is
     array (Natural range <>) of Unsigned_32;
   for U32_Array'Scalar_Storage_Order use High_Order_First;

   --  Initialize the FDT at a given address.
   procedure Init
     (Addr : Address);

   --  Parse the memory map from an FDT.
   procedure Parse_Memory_Map
     (Hdr : aliased Header);

   --  Parge the reg property of a memory node.
   procedure Parse_Mem_Reg
     (Reg                    :        U32_Array;
      Addr_Cells, Size_Cells :        Natural;
      Callback               : access procedure
        (Start : Address;
         Size  : Storage_Count));

   package Iter is
      --  Token iterator state.
      type State is
         record
            Hdr  : access constant Header;
            Addr : Address;
         end record;

      --  Initialize an iterator.
      procedure Init
        (St  :         out State;
         Hdr : aliased     Header);

      --  Get the next token in an iterator.
      procedure Next
        (St  : in out State;
         Tok :    out Token);
   end Iter;
end Fdt;
pragma Preelaborate (Fdt);
