--  Console output functions.

with System; use System;

package Console is
   --  Pointer to a board-specific procedure subprogram to output a
   --  character to the console.
   type Board_Console_Put_Procedure is access procedure
     (Char : Character);

   Board_Console_Put : Board_Console_Put_Procedure := null;

   --  Print a character.
   procedure Put
     (Char : Character);

   --  Print a string.
   procedure Put
     (Str : String);

   --  Print a newline.
   procedure New_Line;

   --  Print a string followed by a newline.
   procedure Put_Line
     (Str : String);

   --  Integer base.
   type Int_Base is range 2 .. 16;

   --  Integer printing alphabet.
   Alphabet : constant String := "0123456789ABCDEF";

   --  Print an unsigned integer of a specific base.
   generic
      type Num is mod <>;
   procedure Put_Unsigned
     (N     : Num;
      Width : Natural;
      Base  : Int_Base := 10);

   --  Kernel panic.
   procedure Panic;
   pragma No_Return (Panic);

   --  Last chance handler.
   procedure Last_Chance_Handler
     (Source_Location : Address;
      Line            : Integer);
   pragma Export (C, Last_Chance_Handler, "__gnat_last_chance_handler");
   pragma No_Return (Last_Chance_Handler);
end Console;
pragma Preelaborate (Console);
