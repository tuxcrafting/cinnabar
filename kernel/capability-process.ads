--  Process capability functions.

limited with Capability.Scheduling_Context;
with Machine_State;

package Capability.Process is
   type Process_Data is
      record
         --  Machine state of the process.
         State : aliased Machine_State.Msd.State;

         --  Page containing the IPC buffer.
         Ipc_Buf : aliased Slot;

         --  Root capability table and page table.
         Table, Page_Table : aliased Slot;

         --  State flags.
         Running, Waiting, Queued, Current : Boolean;

         --  Scheduling context and priority.
         Sched_Ctx : access Scheduling_Context.Scheduler_Data;
         Prio      : Natural;

         --  Scheduler queue links.
         Prev, Next : access Process_Data;
      end record;

   --  Set the scheduling context and priority of a process.
   procedure Set_Scheduling_Context
     (Process   : aliased in out Process_Data;
      Sched_Ctx :         access Scheduling_Context.Scheduler_Data;
      Prio      :                Natural);

   --  Recheck whether the process's capabilities are valid and
   --  potentially (re)schedule it.
   procedure Recheck
     (Process : aliased in out Process_Data);

   --  Context switch to a process.
   procedure Switch
     (Process : aliased Process_Data);
   pragma No_Return (Switch);
end Capability.Process;
pragma Preelaborate (Capability.Process);
