with Console;
with Defs;
with System; use System;
with Types;  use Types;
with Util;

procedure Syscall
  (Num   :                Syscall_Num.Typ;
   Sched : aliased in out Capability.Scheduling_Context.Scheduler_Data) is

   Ipc_Buf : Word_Array
     (0 .. 2 ** Defs.Page_Level_Bits
        (Sched.Current.Ipc_Buf.Pa_Level) /
        (Word_Size / Storage_Unit));
   for Ipc_Buf'Address use Util.To_Virt (Sched.Current.Ipc_Buf.Pa_Addr);
   pragma Import (Ada, Ipc_Buf);
begin
   case Num is
      when Syscall_Num.Debug_Putchar =>
         Console.Put (Character'Val (Ipc_Buf (0)));
   end case;
end Syscall;
