--  Secondary stack handling.

with System.Parameters;
with System.Storage_Elements;

package System.Secondary_Stack is
   package Sp renames System.Parameters;
   package Sse renames System.Storage_Elements;

   --  Size of the secondary stack - 2 KiB.
   Secondary_Stack_Size : constant Natural := 2 * 1024;

   --  Secondary stack mark type.
   type Mark_Id is new Sse.Storage_Offset;

   --  Stack pointer within the stack.
   Stack_Pointer : Mark_Id := 1;

   --  Actual stack data.
   Stack_Data : Sse.Storage_Array
     (1 .. Sse.Storage_Offset (Secondary_Stack_Size));

   --  Allocate an object of a certain size on the secondary stack and
   --  return its address.
   procedure Ss_Allocate
     (Addr : out System.Address;
      Size :     Sse.Storage_Count);

   --  Free a secondary stack. Does nothing here since the secondary
   --  stack is static.
   procedure Ss_Free
     (Stack : in out System.Address);

   --  Mark a location on the secondary stack.
   function Ss_Mark
     return Mark_Id;

   --  Release data on the secondary stack up to a mark.
   procedure Ss_Release
     (Mark : Mark_Id);

   --  Various declarations to make it build.
   type Ss_Stack (Size : Sp.Size_Type) is
      record
         null;
      end record;

   Ss_Pool : Integer;

   Binder_Sec_Stack_Counts : Natural;
   pragma Export (Ada, Binder_Sec_Stack_Counts, "__gnat_binder_ss_count");

   Default_Secondary_Stack_Size : Sp.Size_Type;
   pragma Export (C, Default_Secondary_Stack_Size, "__gnat_default_ss_size");

   Default_Sized_Ss_Pool : System.Address;
   pragma Export (Ada, Default_Sized_Ss_Pool, "__gnat_default_ss_pool");
end System.Secondary_Stack;
pragma Preelaborate (System.Secondary_Stack);
