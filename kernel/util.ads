--  Utility functions.

with System; use System;

package Util is
   --  Get the physical address from a virtual address, and vice
   --  versa.
   function To_Phy
     (Addr : Address)
     return Address;
   function To_Virt
     (Addr : Address)
     return Address;
end Util;
pragma Preelaborate (Util);
pragma Pure (Util);
