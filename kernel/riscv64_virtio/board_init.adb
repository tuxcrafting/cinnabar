with Console;                 use Console;
with Fdt;
with Interrupts;
with Sbi;
with System.Storage_Elements; use System.Storage_Elements;

package body Board_Init is
   procedure Early is
   begin
      Board_Console_Put := Sbi.Console_Putchar'Access;
      Interrupts.Init;
      Interrupts.Enable;
   end Early;

   procedure Late is
   begin
      Fdt.Init (To_Address (16#FFFF800082200000#));
   end Late;
end Board_Init;
