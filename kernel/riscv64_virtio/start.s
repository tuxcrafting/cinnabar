	.section ".start", "ax"

	.global _start
_start:
	/* disable interrupts */
	csrc sstatus, 3

	/* setup paging */
	la t0, init_pt
	srli t1, t0, 12
	li t2, 9
	slli t2, t2, 60
	or t1, t1, t2
	csrw satp, t1

	/* jump to high half */
	la t1, 1f
	ld t1, 0(t1)
	jr t1
1:	.quad start_high

	.section ".init_pt", "aw"
init_pt:
	.rept 512
	/* --DA--XWRV */
	.quad 0b0011001111
	.endr

	.section ".text.start", "ax"

start_high:
	/* unmap zero page */
	sd zero, 0(t0)

	/* setup stack */
	la sp, stack_end

	/* clear .bss */
	.extern zero_bss
	jal ra, zero_bss

	/* jump to Ada code */
	.extern main
	j main

	.section ".bss"

	.align 8
	.lcomm stack, 4096
	.equ stack_end, stack + 4096
