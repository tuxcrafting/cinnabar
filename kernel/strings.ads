--  Basic string handling functions.

package Strings is
   --  Find a substring within a string and return its
   --  position. Returns 0 if it cannot be found.
   function Index
     (Haystack, Needle : String)
     return Natural;
end Strings;
pragma Preelaborate (Strings);
pragma Pure (Strings);
