with Capability.Page_Table.Arch;
with Defs;
with Types;                   use Types;
with Util;
with System.Storage_Elements; use System.Storage_Elements;

package body Capability.Page_Table is
   procedure Map_Addr
     (Table, Addr, Slot_Addr : Address;
      Typ                    : Mapping_Type;
      Index                  : Natural) is
   begin
      Arch.Map (Table, Addr, Typ, Index);
      Update_Addr (Table, Slot_Addr, Index);
   end Map_Addr;

   procedure Unmap_Addr
     (Table : Address;
      Index : Natural) is
   begin
      Map_Addr
        (Table, Null_Address, Null_Address, None, Index);
   end Unmap_Addr;

   procedure Update_Addr
     (Table, Cap_Addr : Address;
      Index           : Natural) is

      Mapped_Caps : aliased Address_Array
        (0 .. 2 ** Defs.Page_Table_Bits / (Address'Size / Storage_Unit) - 1);
      for Mapped_Caps'Address use
        Util.To_Virt (Table + 2 ** Defs.Page_Table_Bits);
      pragma Import (Ada, Mapped_Caps);
   begin
      Mapped_Caps (Index) := Cap_Addr;
   end Update_Addr;

   procedure Activate
     (Table : Address) is
   begin
      Arch.Activate (Table);
   end Activate;

   function Set_Level
     (Cap   : aliased in out Slot;
      Level :                Natural)
     return Errno.Typ is
   begin
      if Cap.Typ /= T.Page_Table then
         return Errno.Invalid_Capability;
      end if;

      if not Cap.Write then
         return Errno.Not_Authorized;
      end if;

      if Level not in 1 .. Defs.Page_Table_Levels then
         return Errno.Invalid_Value;
      end if;

      Arch.Init_Pt (Cap.Pt_Addr, Level);
      Cap.Pt_Level := Level;

      return Errno.Success;
   end Set_Level;

   function Map
     (Cap, Mapped : aliased in out Slot;
      Typ         :                Mapping_Type;
      Index       :                Natural)
     return Errno.Typ is
   begin
      if Cap.Typ /= T.Page_Table then
         return Errno.Invalid_Capability;
      end if;

      if not Cap.Write then
         return Errno.Not_Authorized;
      end if;

      if not Arch.Valid_Index (Cap.Pt_Level, Index) then
         return Errno.Bounds;
      end if;

      declare
         Mapped_Caps : aliased Address_Array
           (0 .. 2 ** Defs.Page_Table_Bits /
              (Address'Size / Storage_Unit) - 1);
         for Mapped_Caps'Address use
           Util.To_Virt (Cap.Pt_Addr + 2 ** Defs.Page_Table_Bits);
         pragma Import (Ada, Mapped_Caps);
      begin
         if Mapped_Caps (Index) /= Mapped'Address
           and then
           (Typ = None
              or else Mapped_Caps (Index) /= Null_Address)
         then
            return Errno.Final;
         end if;
      end;

      case Mapped.Typ is
         when T.Page =>
            if Mapped.Pa_Level /= Cap.Pt_Level - 1 then
               return Errno.Invalid_Capability;
            end if;

            if Mapped.Pa_Table_Index /= Pt_Index_Unmapped xor Typ = None then
               return Errno.Final;
            end if;

            if (Typ = Page_Rw
                  and then
                  (not Mapped.Write
                     or else not Mapped.Read))
              or else
              (Typ = Page_W
                 and then not Mapped.Write)
              or else
              ((Typ = Page_R or else Typ = Page_Rx)
                 and then not Mapped.Read)
            then
               return Errno.Not_Authorized;
            end if;

            Map_Addr
              (Cap.Pt_Addr, Mapped.Pa_Addr,
               (if Typ = None then Null_Address else Mapped'Address),
               Typ, Index);
            if Typ = None then
               Mapped.Pa_Table := Null_Address;
               Mapped.Pa_Table_Index := Pt_Index_Unmapped;
            else
               Mapped.Pa_Table := Cap.Pt_Addr;
               Mapped.Pa_Table_Index := Index;
            end if;

         when T.Page_Table =>
            if Mapped.Pt_Level /= Cap.Pt_Level - 1 then
               return Errno.Invalid_Capability;
            end if;

            if Mapped.Pt_Table_Index /= Pt_Index_Unmapped xor Typ = None then
               return Errno.Final;
            end if;

            if Typ /= Page_Table and then Typ /= None then
               return Errno.Not_Authorized;
            end if;

            Map_Addr
              (Cap.Pt_Addr, Mapped.Pt_Addr,
               (if Typ = None then Null_Address else Mapped'Address),
               Typ, Index);
            if Typ = None then
               Mapped.Pt_Table := Null_Address;
               Mapped.Pt_Table_Index := Pt_Index_Unmapped;
            else
               Mapped.Pt_Table := Cap.Pt_Addr;
               Mapped.Pt_Table_Index := Index;
            end if;

         when others =>
            return Errno.Invalid_Capability;
      end case;

      return Errno.Success;
   end Map;
end Capability.Page_Table;
