package body Memory is
   function Memset
     (Addr : Address;
      Ch   : Integer;
      Size : Storage_Count)
     return Address is

      Arr : aliased Storage_Array (1 .. Size);
      for Arr'Address use Addr;
      pragma Import (Ada, Arr);
   begin
      for I in Arr'Range loop
         Arr (I) := Storage_Element (Ch);
      end loop;

      return Addr;
   end Memset;

   function Memcpy
     (Dest, Src : Address;
      Size      : Storage_Count;
      Revers    : Integer)
     return Address is

      Dest_Arr : aliased Storage_Array (1 .. Size);
      for Dest_Arr'Address use Dest;
      pragma Import (Ada, Dest_Arr);

      Src_Arr : aliased constant Storage_Array (1 .. Size);
      for Src_Arr'Address use Src;
      pragma Import (Ada, Src_Arr);
   begin
      if Revers /= 0 then
         for I in reverse 1 .. Size loop
            Dest_Arr (I) := Src_Arr (I);
         end loop;
      else
         for I in 1 .. Size loop
            Dest_Arr (I) := Src_Arr (I);
         end loop;
      end if;

      return Dest;
   end Memcpy;

   function Strlen
     (Str : Address)
     return Storage_Count is

      Ptr : Address       := Str;
      Len : Storage_Count := 0;
   begin
      Len_Loop : loop
         declare
            Ch : aliased constant Storage_Element;
            for Ch'Address use Ptr;
            pragma Import (Ada, Ch);
         begin
            exit Len_Loop when Ch = 0;
            Ptr := Ptr + 1;
            Len := Len + 1;
         end;
      end loop Len_Loop;

      return Len;
   end Strlen;

   function Memcmp
     (A, B : Address;
      Size : Storage_Count)
     return Integer is

      A_Arr : aliased constant Storage_Array (1 .. Size);
      for A_Arr'Address use A;
      pragma Import (Ada, A_Arr);

      B_Arr : aliased constant Storage_Array (1 .. Size);
      for B_Arr'Address use B;
      pragma Import (Ada, B_Arr);
   begin
      for I in 1 .. Size loop
         if A_Arr (I) /= B_Arr (I) then
            return -1;
         end if;
      end loop;

      return 0;
   end Memcmp;

   procedure Zero_Bss is
      Bss_Start, Bss_End : aliased constant Storage_Element;
      pragma Import (C, Bss_Start, "bss_start");
      pragma Import (C, Bss_End, "bss_end");

      Ignore : Address;
   begin
      Ignore := Memset
        (Bss_Start'Address,
         0,
         Bss_End'Address - Bss_Start'Address);
   end Zero_Bss;

   procedure Get_Kernel_Start_End
     (Start_Addr, End_Addr : out Address) is

      Start_Sym, End_Sym : aliased constant Integer;
      pragma Import (C, Start_Sym, "kernel_start");
      pragma Import (C, End_Sym, "kernel_end");
   begin
      Start_Addr := Start_Sym'Address;
      End_Addr   := End_Sym'Address;
   end Get_Kernel_Start_End;
end Memory;
