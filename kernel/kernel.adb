with Board_Init;
with Console; use Console;
with Init;

--  Entry point of the kernel.
procedure Kernel is
   --  With link-time optimization, GCC apparently requires at least
   --  one explicit reference from code (references from actually
   --  generated code are not counted) to a compilation unit to
   --  actually compile it, so this serves to avoid undefined
   --  reference to memcpy or whatnot errors.
   procedure I_Hate_Gcc;
   pragma Import (C, I_Hate_Gcc, "i_hate_gcc");
begin
   I_Hate_Gcc;

   Board_Init.Early;

   Put_Line ("Cinnabar microkernel (CMK) is booting.");

   Board_Init.Late;

   Init.Finish;
end Kernel;
