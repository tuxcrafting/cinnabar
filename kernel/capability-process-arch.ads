--  Architecture-specific process functions.

package Capability.Process.Arch is
   --  Context switch to a process.
   procedure Switch
     (Process : aliased Process_Data);
   pragma No_Return (Switch);
end Capability.Process.Arch;
pragma Preelaborate (Capability.Process.Arch);
